# About
This is my personal RAT (Remote Administration Tool)/CnC (Command and Control)
utility to be used with my work doing penetration testing. It is written mostly
in C with some of the tools being written in other languages. It has the
capability to do reverse tunneling as well as run any executable.

# Contents
## Library Loader
There is a DLL library that can be compiled that will load the man tool from
a webserver. This DLL is meant to be run from the custom C# DLL loader which
is meant to be used with the .NET InstallUtil to bypass some application
whitelisting.

## Gravity
Gravity is the client application. It phones home to the server over the ssh 
protocol. It also implements some custom syscalls to bypass AV and as such needs
to be rebuilt for each targeted version of windows. This is hopefully going to
change soon.

## BlackHole
Blackhole is the nickname given to the server application.
The server executable implements a basic SSH server with support for reverse
tunnels. It responds to the client becaon and gives out tasks

## Event Horizon
Event Horizon is the web controller built to manage the clients. It uses a mysql
database and sets up the db structure for Blackhole. It is built upon the
Laravel 5 php framework and uses vue js for the frontend.

From the web controller new tasks can be sent, as well as viewing all active
clients, general information about them, and when they last beaconed.


# Dependencies
## Windows
 - cmake
 - vcpkg
 - MSVC
 - visual studio
 - windows
 
## Linux
 - cmake
 - gcc
 
## C Packages
- libssh
- libxml2
- mariadb (server only)



# Disclaimer
I do not condone any illegal activity and this should only be used to legal
purposes.

# PS
An unhealthy amount of nightcore and plastic was consumed in the making of
this project.
