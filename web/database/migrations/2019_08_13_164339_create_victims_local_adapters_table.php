<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVictimsLocalAdaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('victims_local_adapters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('victim_id')->unsigned();
            $table->text('adapter');
            $table->ipAddress('local_ip');
            $table->timestamps();
            $table->foreign('victim_id')->references('id')->on('victims');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('victims_local_adapters');
    }
}
