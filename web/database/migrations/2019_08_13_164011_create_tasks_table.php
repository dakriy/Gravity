<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('victim_id')->unsigned();
            $table->text('command');
            $table->text('parameter');
            $table->boolean('sent')->default(false);
            $table->integer('batch_index')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('victim_id')->references('id')->on('victims');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
