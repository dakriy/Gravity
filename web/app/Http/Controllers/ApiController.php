<?php

namespace App\Http\Controllers;

use App\Victim;
use Illuminate\Support\Facades\Artisan;

class ApiController extends Controller
{
    public function hosts() {
        Artisan::call('beacons:process');
        return ['hosts' => Victim::with('localIps')->orderBy('updated_at', 'desc')->get()];
    }
}
