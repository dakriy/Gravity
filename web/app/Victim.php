<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Victim extends Model
{
    protected $fillable = [
        'host',
        'external',
        'user',
        'os',
        'user_admin',
        'process_admin'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localIps() {
        return $this->hasMany(InternalIP::class);
    }

    /**
     * Get the tasks assigned to a victim
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks() {
        return $this->hasMany(Task::class);
    }
}
