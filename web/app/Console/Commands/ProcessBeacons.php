<?php

namespace App\Console\Commands;

use App\InternalIP;
use App\Victim;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Orchestra\Parser\Xml\Facade as XmlParser;

class ProcessBeacons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'beacons:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process any command beacons waiting in the queue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        dd(storage_path('app'));
//        $files = Storage::disk('local')->get('beacons/1565726742.xml');
        $files = Storage::files('beacons');

        foreach ($files as $file) {
            try {
                $xml = XmlParser::extract(Storage::get($file));
            } catch (\Exception $e) {
                Storage::delete($file);
                continue;
            }
            $beacon = $xml->parse([
                'hostname'                  => ['uses' => 'HostName'],
                'external_ip'               => ['uses' => 'ExternalIP'],
                'user'                      => ['uses' => 'CurrentUser'],
                'operating_system'          => ['uses' => 'OS'],
                'is_user_admin'             => ['uses' => 'UserHasAdmin'],
                'is_process_admin'          => ['uses' => 'ProcessHasAdmin'],
                'internal_ips'              => ['uses' => 'InternalIP[Adapter,IP]'],
            ]);

            /** @var Victim $v */
            $v = Victim::where('host', '=', $beacon['hostname'])
                ->where('user', '=', $beacon['user'])->first();

            if (!$v) {
                // Model does not exist so lets create it
                $v = new Victim([
                    'host'              => $beacon['hostname'],
                    'external'          => $beacon['external_ip'],
                    'user'              => $beacon['user'],
                    'os'                => $beacon['operating_system'],
                    'user_admin'        => ($beacon['is_user_admin'] == 'Y') ? true : false,
                    'process_admin'     => ($beacon['is_process_admin'] == 'Y') ? true : false,
                ]);
                $v->updated_at = Carbon::createFromTimestamp(basename($file, '.xml'));
                $v->save(['timestamps' => false]);

                foreach ($beacon['internal_ips'] as $internal) {
                    $v->localIps()->create([
                        'adapter'       => $internal['Adapter'],
                        'local_ip'      => $internal['IP']
                    ]);
                }
            } else {
                $v->updated_at = Carbon::createFromTimestamp(basename($file, '.xml'));
                $v->host = $beacon['hostname'];
                $v->external = $beacon['external_ip'];
                $v->user = $beacon['user'];
                $v->os = $beacon['operating_system'];
                $v->user_admin = ($beacon['is_user_admin'] == 'Y') ? true : false;
                $v->process_admin = ($beacon['is_process_admin'] == 'Y') ? true : false;

                foreach ($beacon['internal_ips'] as $internal) {
                    $found = false;
                    $update = false;
                    /** @var InternalIP $lip */
                    foreach ($v->localIps()->get() as $lip) {
                        if ($lip->adapter == $internal['Adapter']) {
                            // Then create
                            $found = true;
                            if ($lip->local_ip != $internal['IP']) {
                                $update = true;
                            }
                        }
                    }
                    if (!$found) {
                        $v->localIps()->create([
                            'adapter'       => $internal['Adapter'],
                            'local_ip'      => $internal['IP']
                        ]);
                    }
                    if ($update) {
                        $lip->local_ip = $internal['IP'];
                        $lip->save();
                    }
                }

                $v->save(['timestamps' => false]);
            }
        }
        foreach($files as $file) {
            Storage::delete($file);
        }
    }
}
