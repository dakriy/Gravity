<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternalIP extends Model
{
    protected $table = 'victims_local_adapters';

    protected $fillable = [
        'adapter',
        'local_ip'
    ];

    public function victim() {
        return $this->belongsTo(Victim::class);
    }
}
