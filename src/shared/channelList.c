#include "channelList.h"
#include <stdlib.h>

void freeChannelList(ChannelNode root) {
    ChannelNode tmp;
    while (root) {
        tmp = root;
        root = root->next;
        freeChannelNode(tmp);
    }
}


ChannelNode addChannelNodeWithChannel(ChannelNode *root, ssh_channel c) {
    ChannelNode n = createChannelNode();
    if (!n) {
        return NULL;
    }
    n->c = c;

    *root = appendChannelToList(*root, n);
    return n;
}

ChannelNode appendChannelToList(ChannelNode root, ChannelNode node) {
    if (!root) {
        return node;
    }
    // recursive
    root->next = appendChannelToList(root->next, node);
    return root;
}


ChannelNode removeChannelFromListByNode(ChannelNode root, ChannelNode node) {
    if (!root) {
        return NULL;
    }

    if (root == node) {
        ChannelNode tmp = root->next;
        freeChannelNode(root);
        return tmp;
    }
    // recursive
    root->next = removeChannelFromListByNode(root->next, node);
    return root;
}

ChannelNode removeChannelFromListByChannel(ChannelNode root, ssh_channel c) {
    if (!root) {
        return NULL;
    }

    if (root->c == c) {
        ChannelNode tmp = root->next;
        freeChannelNode(root);
        return tmp;
    }
    // recursive
    root->next = removeChannelFromListByChannel(root->next, c);
    return root;
}

void freeChannelNode(ChannelNode c) {
    if (!c) {
        return;
    }

    if (c->attached) {
        close(c->socket);
    }

    if (c->c) {
        ssh_channel_close(c->c);
        ssh_channel_free(c->c);
    }

    free(c);
}

int getChannelListCount(ChannelNode root) {
    int count = 0;
    while (root) {
        ++count;
        root = root->next;
    }
    return count;
}

ChannelNode createChannelNode() {
    return (ChannelNode) calloc(1, sizeof(struct channelNode));
}

ChannelNode getChannelNodeFromListByChannel(ChannelNode root, ssh_channel c) {
    while (root) {
        if (root->c == c) {
            return root;
        }
        root = root->next;
    }
    return NULL;
}
