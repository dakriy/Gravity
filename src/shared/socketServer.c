#include "socketServer.h"
#include <stdio.h>

SOCKET_DESCRIPTOR make_listen_socket(uint16_t port) {
    // Most of the calls are the same on linux as in windows for the networking
    SOCKET_DESCRIPTOR sock;
    struct sockaddr_in name;

    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        return SOCKET_CREATION_FAILED;
    }
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    if (bind(sock, (struct sockaddr *) &name, sizeof(name)) < 0) {
        return SOCKET_CREATION_FAILED;
    }
    return sock;
}

void *get_in_addr(struct sockaddr *sa) {
    // Most of the calls are the same on linux as in windows for the networking
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *) sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *) sa)->sin6_addr);
}

SOCKET_DESCRIPTOR make_connect_socket(uint16_t port) {
    // Most of the calls are the same on linux as in windows for the networking
    SOCKET_DESCRIPTOR sock = SOCKET_CREATION_FAILED;
    struct addrinfo hints, *servinfo = NULL, *p = NULL;
    char portStr[6];
    char s[INET6_ADDRSTRLEN];
    snprintf(portStr, 6, "%d", port);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if (getaddrinfo("localhost", portStr, &hints, &servinfo) != 0) {
        return SOCKET_CREATION_FAILED;
    }

    // loop through all the results and connect to the first we can
    for (p = servinfo; p != NULL; p = p->ai_next) {
        if ((sock = socket(p->ai_family, p->ai_socktype,
                           p->ai_protocol)) == -1) {
            continue;
        }

        if (connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
            close(sock);
            continue;
        }

        break;
    }

    if (!p) {
        return SOCKET_CREATION_FAILED;
    }

    inet_ntop(p->ai_family, get_in_addr((struct sockaddr *) p->ai_addr),
              s, sizeof s);

    freeaddrinfo(servinfo);

    return sock;
}

int handle_socket_tunnel_bindings(ssh_session session, ChannelNode *list,
                                  void (*handleSocketCallback)(ChannelNode, ssh_session)) {
    // Most of the calls are the same on linux as in windows for the networking
    ChannelNode node = *list;

    while (node) {
        if (node->attached) {
            // copy over the master set to the reading set
            fd_set read_fd_set;
            FD_ZERO(&read_fd_set);
            FD_SET(node->socket, &read_fd_set);

            // Setup timeout time
            struct timeval timeout;
            timeout.tv_sec = 0;
            timeout.tv_usec = 0;

            if (select(FD_SETSIZE + 1, &read_fd_set, NULL, NULL, &timeout) <= 0) {
                node = node->next;
                continue;
            }
            /* Service all the sockets with input pending. */
            // We know there will only be one socket, because I have limited one socket per channel? I guess
            if (FD_ISSET(node->socket, &read_fd_set)) {
                handleSocketCallback(node, session);
            }
        }
        node = node->next;
    }
    return 0;
}
