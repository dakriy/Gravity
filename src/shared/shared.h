#ifndef GRAVITY_SHARED_H
#define GRAVITY_SHARED_H

#include <ctype.h>

#define USP (unsigned char *)
#define US (unsigned char)
#define SCP (char *)

#define BEACON_HEADER_SIZE (sizeof(size_t))

inline void lowerCaseString(unsigned char *p) {
    for (; *p; ++p) *p = tolower(*p);
}

#endif //GRAVITY_SHARED_H
