#ifndef GRAVITY_SOCKETSERVER_H
#define GRAVITY_SOCKETSERVER_H

#include <stdint.h>
#include <limits.h>
#include "channelList.h"

#define SOCKET_READ_BUFFER_SIZE 1024 * 4

#ifdef _WIN32

#include <WinSock2.h>
#include <WS2tcpip.h>

typedef SOCKET SOCKET_DESCRIPTOR;
#define SOCKET_CREATION_FAILED INT_MAX
#else
#include <arpa/inet.h>
typedef int SOCKET_DESCRIPTOR;
#define SOCKET_CREATION_FAILED INT32_MAX
#endif

/**
 * Creates a socket to listen on a port
 *
 * @param port to listen on
 * @return the SOCKET_DESCRIPTOR of the created socket
 */
SOCKET_DESCRIPTOR make_listen_socket(uint16_t port);

/**
 * Creates a socket that connects to a port on localhost
 *
 * @param port to connect to on localhost
 * @return the SOCKET_DESCRIPTOR of the created socket
 */
SOCKET_DESCRIPTOR make_connect_socket(uint16_t port);

/**
 * Handles receiving data from the socket and sending along to the ssh channel
 * This does modify the list, for example if the socket or the channel seem to be closed it will
 * remove them from the list and clean them up
 *
 * @param list
 * @param handleSocketCallback takes the channel node of the node with data on it, and the ssh session it is attached to
 * @return 0 on success; < 0 on error
 */
int handle_socket_tunnel_bindings(ssh_session session, ChannelNode *list,
                                  void (*handleSocketCallback)(ChannelNode, ssh_session));

#endif //GRAVITY_SOCKETSERVER_H
