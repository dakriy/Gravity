#ifndef GRAVITY_BASE64_H
#define GRAVITY_BASE64_H

/**
 * Encodes data from data, up to len, and puts it into str up to strLen
 * returns -1 if not successful. This function allocates str which later needs to be freed
 * @param data
 * @param str
 * @return
 */
unsigned char *b64encode(const unsigned char *clear, int len);

/**
 * Takes a C-String and decodes the base64 data into the buffer pointer
 * This function allocates buffer that later needs to be freed.
 * Returns -1 if not successful
 * @param data
 * @param buffer
 * @return size of data
 */
int b64decode(const unsigned char *code, unsigned char **ptr);

#endif //GRAVITY_BASE64_H
