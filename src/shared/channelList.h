#ifndef GRAVITY_CHANNELLIST_H
#define GRAVITY_CHANNELLIST_H

#ifdef _WIN32

#include <WinSock2.h>

#define close closesocket
#endif

#include <stdbool.h>
#include <libssh/libssh.h>
#include <libssh/callbacks.h>

// Awful architecture me thinks, but I'm in a hurry, so don't have time to fix it
// TODO: Improve the tunnel/channel separation architecture...
// Later edit: I mean it's not ALL that bad...
typedef struct channelNode {
    struct channelNode *next;
    ssh_channel c;
    ssh_channel_callbacks cb;
    int lport;
    int rport;

    bool closeChannel;

    bool attached;
#ifdef _WIN32
    SOCKET socket;
#else
    int socket;
#endif
} *ChannelNode;

/**
 * Frees a channel list and closes all channels in it
 *
 * @param root the root node to free the list from
 */
void freeChannelList(ChannelNode root);

/**
 * Add a node to the end of a channel list
 * The root node can be null, but usage must set the root node equal to the returned node
 *
 * @param root root node, or any node in the list
 * @param node the node to add
 * @return The root node, in case root was null
 */
ChannelNode appendChannelToList(ChannelNode root, ChannelNode node);

/**
 * Add a channel to the list.
 * This function creates a node for the channel,
 * places the channel into the node,
 * and then appends the node to the node list.
 *
 * @param root Pointer to the root node (The pointer can point to a pointer that points to null, but cannot be null)
 * @param c the channel to add
 * @return returns the created node on success or null on error
 */
ChannelNode addChannelNodeWithChannel(ChannelNode *root, ssh_channel c);

/**
 * Removes a specified channel from the channel list
 * Can change the root node, so the root node needs to be set equal to this function.
 *
 * @param root Root node to start searching from
 * @param c The channel to remove
 * @return Returns the root node, proper usage sets the root node equal to this function.
 */
ChannelNode removeChannelFromListByChannel(ChannelNode root, ssh_channel c);

/**
 * Removes a specified node from the channel list
 * Can change the root node, so the root node needs to be set equal to this function.
 *
 * @param root Root node to start searching from
 * @param node the node to remove
 * @return Returns the root node, proper usage sets the root node equal to this function.
 */
ChannelNode removeChannelFromListByNode(ChannelNode root, ChannelNode node);

/**
 * Returns the number of nodes in the list starting from root
 *
 * @param root the node to start counting from
 * @return number of nodes
 */
int getChannelListCount(ChannelNode root);

/**
 * Creates a new node 0 initialized.
 * This needs to be freed by the caller with a call to freeChannelNode
 *
 * @return Returns a new channel node on success, null on failure
 */
ChannelNode createChannelNode();

/**
 * Returns a node in a specified list by channel
 *
 * @param root The node to start searching from
 * @param c the channel to search for
 * @return the channel node if found, NULL if not found
 */
ChannelNode getChannelNodeFromListByChannel(ChannelNode root, ssh_channel c);

/**
 * Frees and cleans up after a channel node.
 * DOES NOT UNHOOK FROM THE LIST, for that functionality use removeChannelFromListByNode
 *
 * @param c the node to free
 */
void freeChannelNode(ChannelNode c);

#endif //GRAVITY_CHANNELLIST_H
