#include "base64.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/**
 * SHAMELESSLY STOLEN FROM THE EXIM SOURCE and slightly modified
 * This is not the vulnerable version of that source, I made sure of that.
 *
 * If you don't know what I'm talking about and want to, it's interesting read
 * https://devco.re/blog/2018/03/06/exim-off-by-one-RCE-exploiting-CVE-2018-6789-en/
 */

/* This function decodes a string in base 64 format as defined in RFC 2045
(MIME) and required by the SMTP AUTH extension (RFC 2554). The decoding
algorithm is written out in a straightforward way. Turning it into some kind of
compact loop is messy and would probably run more slowly.
Arguments:
  code        points to the coded string, zero-terminated
  ptr         where to put the pointer to the result, which is in
			  allocated store, and zero-terminated
Returns:      the number of bytes in the result,
			  or -1 if the input was malformed
Whitespace in the input is ignored.
A zero is added on to the end to make it easy in cases where the result is to
be interpreted as text. This is not included in the count. */

static unsigned char dec64table[] = {
        255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, /*  0-15 */
        255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, /* 16-31 */
        255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 62, 255, 255, 255, 63, /* 32-47 */
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 255, 255, 255, 255, 255, 255, /* 48-63 */
        255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, /* 64-79 */
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 255, 255, 255, 255, 255, /* 80-95 */
        255, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, /* 96-111 */
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 255, 255, 255, 255, 255  /* 112-127*/
};

int
b64decode(const unsigned char *code, unsigned char **ptr) {
    int x, y;
    unsigned char *result;

    {
        int l = (int) strlen((char *) code);
        *ptr = result = (unsigned char *) malloc(1 + l / 4 * 3 + l % 4);
    }

    /* Each cycle of the loop handles a quantum of 4 input bytes. For the last
    quantum this may decode to 1, 2, or 3 output bytes. */

    while ((x = *code++) != 0) {
        if (isspace(x)) continue;
        /* debug_printf("b64d: '%c'\n", x); */

        if (x > 127 || (x = dec64table[x]) == 255) return -1;

        while (isspace(y = *code++));
        /* debug_printf("b64d: '%c'\n", y); */
        if (y > 127 || (y = dec64table[y]) == 255)
            return -1;

        *result++ = (x << 2) | (y >> 4);
        /* debug_printf("b64d:      -> %02x\n", result[-1]); */

        while (isspace(x = *code++));
        /* debug_printf("b64d: '%c'\n", x); */
        if (x == '=')        /* endmarker, but there should be another */
        {
            while (isspace(x = *code++));
            /* debug_printf("b64d: '%c'\n", x); */
            if (x != '=') return -1;
            while (isspace(y = *code++));
            if (y != 0) return -1;
            /* debug_printf("b64d: DONE\n"); */
            break;
        } else {
            if (x > 127 || (x = dec64table[x]) == 255) return -1;
            *result++ = (y << 4) | (x >> 2);
            /* debug_printf("b64d:      -> %02x\n", result[-1]); */

            while (isspace(y = *code++));
            /* debug_printf("b64d: '%c'\n", y); */
            if (y == '=') {
                while (isspace(y = *code++));
                if (y != 0) return -1;
                /* debug_printf("b64d: DONE\n"); */
                break;
            } else {
                if (y > 127 || (y = dec64table[y]) == 255) return -1;
                *result++ = (x << 6) | y;
                /* debug_printf("b64d:      -> %02x\n", result[-1]); */
            }
        }
    }

    *result = 0;
    return result - *ptr;
}


/*************************************************
*          Encode byte-string in base 64         *
*************************************************/

/* This function encodes a string of bytes, containing any values whatsoever,
in base 64 as defined in RFC 2045 (MIME) and required by the SMTP AUTH
extension (RFC 2554). The encoding algorithm is written out in a
straightforward way. Turning it into some kind of compact loop is messy and
would probably run more slowly.
Arguments:
  clear       points to the clear text bytes
  len         the number of bytes to encode
Returns:      a pointer to the zero-terminated base 64 string, which
			  is in working store
*/

static unsigned char *enc64table =
        (unsigned char *)
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

unsigned char *
b64encode(const unsigned char *clear, int len) {
    unsigned char *code = (unsigned char *) malloc(4 * ((len + 2) / 3) + 1);
    unsigned char *p = code;

    while (len-- > 0) {
        int x, y;

        x = *clear++;
        *p++ = enc64table[(x >> 2) & 63];

        if (len-- <= 0) {
            *p++ = enc64table[(x << 4) & 63];
            *p++ = '=';
            *p++ = '=';
            break;
        }

        y = *clear++;
        *p++ = enc64table[((x << 4) | ((y >> 4) & 15)) & 63];

        if (len-- <= 0) {
            *p++ = enc64table[(y << 2) & 63];
            *p++ = '=';
            break;
        }

        x = *clear++;
        *p++ = enc64table[((y << 2) | ((x >> 6) & 3)) & 63];

        *p++ = enc64table[x & 63];
    }

    *p = 0;

    return code;
}
