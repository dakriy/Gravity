#ifndef GRAVITY_AVBYPASS_H
#define GRAVITY_AVBYPASS_H

// Not currently used.
// TODO: implement more av bypass features maybe?

#include <stdbool.h>

struct mapped_memory {
    void *addr;
    int len;
};

bool debuggerAttached();

bool avPresent(const char *programName);

bool executeShellCode(char shellcode[], int len);

void shellCodeThread(struct mapped_memory *m);

#endif //GRAVITY_AVBYPASS_H
