#ifndef GRAVITY_RECON_H
#define GRAVITY_RECON_H

#include "common.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32

#include <Windows.h>
#include <lmcons.h>
#include <WinInet.h>
#include <ShlObj.h>
#include <iphlpapi.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>

#else
#include <unistd.h>
#include <limits.h>
#endif

struct LocalIPElement {
    unsigned char *name;
    unsigned char *ip;
};

typedef struct LocalIPElement *LocalIPList;

/**
 * Returned string must be free'd with free
 * @return
 */
unsigned char *getHostName();

/**
 * Gets the list of all the IP's
 * the IpList must be freed by the caller
 *
 * @param ipList
 * @return Number of IP's found, must be passed to freeInternalIpList when freeing
 */
unsigned getInternalIPs(LocalIPList *ipList);

/**
 * Free's the ip list memory
 *
 * @param ipList
 * @param len
 */
void freeInternalIpList(LocalIPList ipList, size_t len);

/**
 * The returned string must be free'd by the caller
 * @return
 */
unsigned char *getExternalIP();

/**
 * The returned string should be free'd by the caller
 * @return
 */
unsigned char *getCurrentUser();

/**
 * The returned string should be free'd by the caller
 * @return
 */
unsigned char *getOS();

/**
 * No freeing necessary for this function.
 * @return Y if yes, N if no
 */
unsigned char getAdminAccess();

/**
 * No freeing necessary for this function
 * @return  Y if yes, N if no
 */
unsigned char isElevated();

#endif //GRAVITY_RECON_H
