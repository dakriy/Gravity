#ifndef GRAVITY_SSHCLIENT_H
#define GRAVITY_SSHCLIENT_H

#include <libssh/libssh.h>
#include "common.h"

/**
 * Does the verifying of the public key to make sure the connection is not being tampered with
 * @param session
 * @return 0 on success; < 0 on error
 */
int verify_known_host(ssh_session session);

/**
 * Initiates an SSH session
 * Session should be closed by caller with endSession
 *
 * @return an ssh session on success; NULL on error
 */
ssh_session initiateSession();

/**
 * Closes and cleans up an ssh session
 *
 * @param s ssh_session to end
 */
void endSession(ssh_session s);

/**
 * Creates a normal ssh channel for communication and beaconing
 * ssh session should be freed by the caller with closeChannel
 *
 * @param s ssh_session to create the channel on
 * @return a created SSH session on success; NULL on error
 */
ssh_channel createDefaultChannel(ssh_session s);

/**
 * Closes and frees a created ssh channel
 *
 * @param c ssh_channel to close
 */
void closeChannel(ssh_channel c);

#endif //GRAVITY_SSHCLIENT_H
