#include "common.h"
#include "../shared/env.h"

int xorWithApplicationKey(const unsigned char *enc, unsigned char *output, int outBuffLen) {
    const char xorKey[] = APPLICATION_KEY;
    int encLen = (int) strlen(SCP enc);
    size_t xorKeyLen = sizeof(xorKey) / sizeof(xorKey[0]);

    if (encLen > outBuffLen) {
        return -1;
    }

    for (int i = 0; i < encLen; ++i) {
        output[i] = US enc[i] ^ US xorKey[i % xorKeyLen];
    }

    return SSH_OK;
}


unsigned char *decryptStr(const unsigned char *str) {
    size_t len = strlen(SCP str);
    unsigned char *unencStr = USP malloc((len + 1) * sizeof(unsigned char));
    if (unencStr == NULL) {
        return NULL;
    }
    xorWithApplicationKey(str, unencStr, (int) len);
    unencStr[len] = 0x00;
    return unencStr;
}

void sleep(unsigned long long milliseconds) {
#ifdef _WIN32
    Sleep((DWORD) milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}
