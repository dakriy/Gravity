#include "tunnelManager.h"

#ifdef _WIN32

#include <process.h>
#include <WinSock2.h>

#else
#endif

#include "sshClient.h"
#include "globals.h"


// Tunnel Node Specifiers
typedef struct tunnelNode {
    int lPort;
    int rPort;
    bool closeTunnel;
    bool requested;
    struct tunnelNode *next;
} *TunnelNode;

static TunnelNode removeTunnelFromListByNode(TunnelNode root, TunnelNode node);

static void freeTunnelList(TunnelNode root);

static TunnelNode appendTunnelToList(TunnelNode root, TunnelNode node);

static TunnelNode createTunnelNode();


// Internal method specifiers
static void acquireTunnelListLock();

static void releaseTunnelListLock();

static void handleTunnels(void *);

static void doTunnels(ssh_session s);

static int requestTunnel(ssh_session s, TunnelNode n);

static void processTunnelChanges(ssh_session s);

static int
handleTunnelChannelData(ssh_session session, ssh_channel channel, void *data, uint32_t len, int is_stderr,
                        void *userdata);

static void handleTunnelClose(ssh_session session, ssh_channel channel, void *userdata);

static void clientSocketHandleCallback(ChannelNode node, ssh_session session);

static void openNewChannelsForATunnel(ssh_session session);


// Local variables
static HANDLE tunnelListMutex = NULL;
static ChannelNode c_queue = NULL;
static TunnelNode t_queue = NULL;
static HANDLE threadHandle = NULL;


static struct ssh_channel_callbacks_struct channelCallbacksStruct = {
        .channel_data_function = &handleTunnelChannelData,
        .channel_close_function = &handleTunnelClose,
        .channel_eof_function = &handleTunnelClose
};

int setupTunnelManager() {
#ifdef _WIN32
    WSADATA wsa;

    // You know... if we can't initialize the sockets library, then what's the point?
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        WSACleanup();
        return -1;
    }

    tunnelListMutex = CreateMutexW(NULL, FALSE, NULL);
    if (!tunnelListMutex) {
        return -1;
    }
    threadHandle = (HANDLE) _beginthread(handleTunnels, 0, NULL);
    return 0;
#else
    // TODO: Implement on linux devices
    return -1
#endif
}

void handleTunnels(void *param) {
    (void) param;
    do {
        acquireTunnelListLock();
        while (!t_queue && !done) {
            releaseTunnelListLock();
            // Nothing yet just sit and sleep
            sleep(100);
            acquireTunnelListLock();
        }
        releaseTunnelListLock();

        if (!done) {
            ssh_session tunnelSession = initiateSession();
            if (!tunnelSession) {
                // Retry the connection?
                continue;
            }

            // We are to quit when this function returns...
            doTunnels(tunnelSession);

            // No more tunnels
            // Free tunnel queue if we are exiting due to the done flag
            freeChannelList(c_queue);
            c_queue = NULL;

            acquireTunnelListLock();
            freeTunnelList(t_queue);
            t_queue = NULL;
            releaseTunnelListLock();

            endSession(tunnelSession);
        }
    } while (!done);
#ifdef _WIN32
    // _endthread given to terminate
    _endthread();
#else
    // TODO: Implement the threading on linux
#endif
}

int cleanupTunnelManager() {
#ifdef _WIN32
    if (threadHandle)
        WaitForSingleObject(threadHandle, INFINITE);

    if (tunnelListMutex)
        CloseHandle(tunnelListMutex);

    WSACleanup();

    return 0;
#else
    // TODO: Implement cleanup for linux
    return 0;
#endif
}


void startTunnel(int lport, int rport) {
    TunnelNode tmp = createTunnelNode();
    if (!tmp) return;
    tmp->lPort = lport;
    tmp->rPort = rport;
    acquireTunnelListLock();
    t_queue = appendTunnelToList(t_queue, tmp);
    releaseTunnelListLock();
}

void closeTunnel(int lport) {
    acquireTunnelListLock();
    TunnelNode q = t_queue;
    while (q) {
        if (q->lPort == lport) {
            q->closeTunnel = true;
            break;
        }
        q = q->next;
    }
    releaseTunnelListLock();
}

void processTunnelChanges(ssh_session s) {
    // Process all tunnels
    TunnelNode current = t_queue;
    acquireTunnelListLock();
    while (current) {
        if (!current->requested) {
            // We need to request the channel
            if (requestTunnel(s, current) < 0) {
                current->closeTunnel = true;
            }
        }

        if (current->closeTunnel) {
            // Mark all open channels in the tunnel for closing
            ChannelNode cur = c_queue;
            while (cur) {
                if (cur->lport == current->lPort) {
                    cur->closeChannel = true;
                }
                cur = cur->next;
            }

            if (ssh_channel_cancel_forward(s, "localhost", current->rPort) == SSH_ERROR) {
                // Ohhh shoot. ohh geez Rick.. owwuuhhhhh... Hmm... what do I do here?? Restart the session?
                // No... I don't want to do that here... lets ignore it because we are closing the tunnel anyway,
                // and if the session really did die on us, it will restart it later anyway
            }

            // Remove the tunnel item from the list
            TunnelNode tmp = current->next;
            t_queue = removeTunnelFromListByNode(t_queue, current);
            current = tmp;
            continue;
        }
        current = current->next;
    };
    releaseTunnelListLock();

    // Look for any new channels on an active tunnel
    openNewChannelsForATunnel(s);

    // Process all channels
    ChannelNode cur = c_queue;
    while (cur) {
        if (cur->closeChannel) {
            ChannelNode tmp = cur->next;
            c_queue = removeChannelFromListByNode(c_queue, cur);
            cur = tmp;
            continue;
        }
        cur = cur->next;
    };
}

void doTunnels(ssh_session s) {
    ssh_event event = ssh_event_new();
    if (!event) {
        return;
    }

    if (ssh_event_add_session(event, s) == SSH_ERROR) {
        ssh_event_remove_session(event, s);
        ssh_event_free(event);
        return;
    }
    acquireTunnelListLock();
    while (!done && t_queue) {
        releaseTunnelListLock();
        // Check for new tunnels every 100ms
        int ret = ssh_event_dopoll(event, 100);
        // Probably disconnected so we done
        if (ret == SSH_ERROR) {
            break;
        }
        processTunnelChanges(s);
        handle_socket_tunnel_bindings(s, &c_queue, clientSocketHandleCallback);
        acquireTunnelListLock();
    }
    releaseTunnelListLock();
    ssh_event_remove_session(event, s);
    ssh_event_free(event);
}

int requestTunnel(ssh_session s, TunnelNode n) {
    int rc;
    // "localhost" means to listen on all protocol families supported by
    //  the SSH implementation on loopback addresses only ([RFC3330] and
    //  [RFC3513]).
    rc = ssh_channel_listen_forward(s, "localhost", n->rPort, &n->rPort);
    n->requested = true;
    if (rc != SSH_OK) {
        return -1;
    }
    return 0;
}

void acquireTunnelListLock() {
#ifdef _WIN32
    WaitForSingleObject(tunnelListMutex, INFINITE);
#else
    // TODO: Implement linux mutexes
#endif
}

void releaseTunnelListLock() {
#ifdef _WIN32
    ReleaseMutex(tunnelListMutex);
#else
    // TODO: Implement linux mutexes
#endif
}

void clientSocketHandleCallback(ChannelNode node, ssh_session session) {
    (void) session;
    /* Data arriving on an already-connected socket. */

    // Write the data to the ssh channel...
    // buffer size of 4k?
    char BUFFER[SOCKET_READ_BUFFER_SIZE];
    int nbytes = recv(node->socket, BUFFER, SOCKET_READ_BUFFER_SIZE, 0);
    if (nbytes <= 0) {
        // Close the connection I suppose.
        node->closeChannel = true;
    } else {
        // Send that delicious data
        if (ssh_channel_write(node->c, BUFFER, nbytes) == SSH_ERROR) {
            // Tunnel is down, close the channel...
            node->closeChannel = true;
        }
    }
}

int handleTunnelChannelData(ssh_session session, ssh_channel channel, void *data, uint32_t len, int is_stderr,
                            void *userdata) {
    (void) session;
    (void) channel;
    (void) is_stderr;
    ChannelNode node = (ChannelNode) userdata;
    if (send(node->socket, data, len, 0) < 0) {
        node->closeChannel = true;
    }
    return len;
}

void handleTunnelClose(ssh_session session, ssh_channel channel, void *userdata) {
    (void) session;
    (void) channel;
    ChannelNode node = (ChannelNode) userdata;
    node->closeChannel = true;
}

TunnelNode removeTunnelFromListByNode(TunnelNode root, TunnelNode node) {
    if (!root) {
        return NULL;
    }

    if (root == node) {
        TunnelNode tmp = root->next;
        free(root);
        return tmp;
    }

    root->next = removeTunnelFromListByNode(root->next, node);
    return root;
}

void freeTunnelList(TunnelNode root) {
    TunnelNode tmp;
    while (root) {
        tmp = root;
        root = root->next;
        free(tmp);
    }
}

TunnelNode appendTunnelToList(TunnelNode root, TunnelNode node) {
    if (!root) {
        return node;
    }

    root->next = appendTunnelToList(root->next, node);
    return root;
}

TunnelNode createTunnelNode() {
    return (TunnelNode) calloc(1, sizeof(struct tunnelNode));
}

void openNewChannelsForATunnel(ssh_session session) {
    int rPort = 0;
    // Poll for any new opening any new channels for any tunnels
    ssh_channel c = ssh_channel_accept_forward(session, 0, &rPort);
    if (!c) {
        return;
    }

    TunnelNode tn = t_queue;
    while (tn) {
        if (tn->rPort == rPort) {
            ChannelNode n = addChannelNodeWithChannel(&c_queue, c);
            // Copy over tunnel information
            n->lport = tn->lPort;
            n->rport = tn->rPort;

            // Set channel callbacks
            n->cb = &channelCallbacksStruct;
            ssh_callbacks_init(&channelCallbacksStruct)
            n->cb->userdata = n;
            ssh_set_channel_callbacks(n->c, n->cb);

            // Bind the socket
            n->socket = make_connect_socket(n->lport);
            if (n->socket == SOCKET_CREATION_FAILED) {
                // Abort everything.
                n->closeChannel = true;
                return;
            }

            n->attached = true;
        }
        tn = tn->next;
    }
}
