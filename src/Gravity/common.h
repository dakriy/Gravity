#ifndef GRAVITY_COMMON_H_
#define GRAVITY_COMMON_H_
#undef UNICODE

#include <libssh/libssh.h>
#include "../shared/shared.h"

#ifdef _WIN32

#include <Windows.h>

#else
#include <unistd.h>
#endif

/**
 * Encrypts or decrypts data with a repeating key xor.
 * Returns 0 on success.
 *
 * @param enc
 * @param output
 * @param outBuffLen
 * @return
 */
int xorWithApplicationKey(const unsigned char *enc, unsigned char *output, int outBuffLen);

/**
 * Decrypts repeating key xor string and preserves stringiness (null byte at end).
 * This function allocates memory that should be deallocated later.
 *
 * @param str
 * @return decrypted string
 */
unsigned char *decryptStr(const unsigned char *str);

/**
 * Multi-platform sleep
 *
 * @param milliseconds
 */
void sleep(unsigned long long milliseconds);

#endif /* GRAVITY_COMMON_H_ */