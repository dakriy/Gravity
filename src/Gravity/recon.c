#include "recon.h"

unsigned char *getHostName() {
#ifdef _WIN32
    unsigned char *nameBuf = NULL;
    DWORD nameBufCharCount = 0;
    // Should fail, we do this twice as it will give the size that the buffer needs to be
    GetComputerNameEx(ComputerNameDnsFullyQualified, NULL, &nameBufCharCount);
    nameBuf = USP malloc(nameBufCharCount * sizeof(unsigned char));
    if (!nameBuf) {
        free(nameBuf);
        return NULL;
    }
    if (!GetComputerNameEx(ComputerNameDnsFullyQualified, SCP nameBuf, &nameBufCharCount)) {
        free(nameBuf);
        return NULL;
    }
    return nameBuf;
#else
    // TODO: Implement and test for linux
    unsigned char * hostname = USP malloc(HOST_NAME_MAX * sizeof(unsigned char));
    gethostname(SCP hostname, HOST_NAME_MAX);
    return hostname;
#endif
}

unsigned getInternalIPs(LocalIPList *ipList) {
#ifdef _WIN32
    *ipList = NULL;
#ifdef DEBUG
    // If GetAdaptersAddresses is called while debugging on a computer where
    // trend micro is running, it will throw an exception, so we just won't run it
    // and return a size of 0 with a null ipList.
    // I'm on a computer where trend micro is running and can't disable it, actually... probably could
    // but too lazy to.
    // So dumb.
    return 0;
#endif
    IP_ADAPTER_ADDRESSES *adapter_addresses = NULL;
    IP_ADAPTER_ADDRESSES *adapter = NULL;

    // Start with a 16 KB buffer and resize if needed -
    // multiple attempts in case interfaces change while
    // we are in the middle of querying them.
    DWORD adapter_addresses_buffer_size = 16000;
    for (int attempts = 0; attempts != 3; ++attempts) {
        adapter_addresses = (IP_ADAPTER_ADDRESSES *) malloc(adapter_addresses_buffer_size);
        if (!adapter_addresses) {
            return -1;
        }

        DWORD error = GetAdaptersAddresses(
                AF_INET,
                (unsigned) GAA_FLAG_SKIP_ANYCAST |
                (unsigned) GAA_FLAG_SKIP_MULTICAST |
                (unsigned) GAA_FLAG_SKIP_DNS_SERVER,
                NULL,
                adapter_addresses,
                &adapter_addresses_buffer_size);

        if (ERROR_SUCCESS == error) {
            // We're done here, people!
            break;
        } else if (ERROR_BUFFER_OVERFLOW == error) {
            // Try again with the new size
            free(adapter_addresses);
            adapter_addresses = NULL;

            continue;
        } else {
            // Unexpected error code - log and throw
            free(adapter_addresses);
            return -1;
        }
    }

    // Get the size of the linked list
    unsigned size = 0;
    for (adapter = adapter_addresses; NULL != adapter; adapter = adapter->Next) ++size;

    LocalIPList ips = (LocalIPList) malloc(size * sizeof(struct LocalIPElement));

    if (!ips) {
        free(adapter_addresses);
        return -1;
    }

    unsigned index = 0;

    for (adapter = adapter_addresses; NULL != adapter; adapter = adapter->Next) {
        // Skip loopback adapters
        if (IF_TYPE_SOFTWARE_LOOPBACK == adapter->IfType) continue;

        IP_ADAPTER_UNICAST_ADDRESS *address = adapter->FirstUnicastAddress;
        SOCKADDR_IN *ipv4 = (SOCKADDR_IN *) address->Address.lpSockaddr;
        PWSTR ipaddrew = (PWSTR) malloc(INET_ADDRSTRLEN * sizeof(WCHAR));
        if (!ipaddrew) {
            free(ipaddrew);
            continue;
        }
        if (!InetNtopW(AF_INET, &(ipv4->sin_addr), ipaddrew, INET_ADDRSTRLEN)) {
            free(ipaddrew);
            continue;
        }

        WCHAR *s = adapter->FriendlyName;
        // Only 32 adapters can be processed
#define FRIENDLY_NAME_LENGTH 32
        ips[index].name = USP malloc(FRIENDLY_NAME_LENGTH);
        if (!ips[index].name) {
            free(ips[index].name);
            continue;
        }
        snprintf(SCP ips[index].name, FRIENDLY_NAME_LENGTH, "%ws", s);
#undef FRIENDLY_NAME_LENGTH

        ips[index].ip = USP malloc(INET_ADDRSTRLEN);
        if (!ips[index].name) {
            free(ips[index].ip);
            continue;
        }
        snprintf(SCP ips[index].ip, INET_ADDRSTRLEN, "%ws", ipaddrew);
        free(ipaddrew);
        index++;
    }
    free(adapter_addresses);
    *ipList = ips;
    return index;
#else
    // TODO: Implement for linux devices
    return -1;
#endif
}

void freeInternalIpList(LocalIPList ipList, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        free(ipList[i].name);
        free(ipList[i].ip);
    }
    free(ipList);
}

unsigned char *getExternalIP() {
#ifdef _WIN32
    HINTERNET net = InternetOpen("",
                                 INTERNET_OPEN_TYPE_PRECONFIG,
                                 NULL,
                                 NULL,
                                 0);
    if (!net) {
        return NULL;
    }

    // TODO: Change this to be on the webserver portion instead of depending on external sites
    HINTERNET conn = InternetOpenUrl(net,
                                     "http://myexternalip.com/raw",
                                     NULL,
                                     0,
                                     INTERNET_FLAG_RELOAD,
                                     0);
    if (!conn) {
        InternetCloseHandle(net);
        return NULL;
    }

#define IP_BUFFER_SIZE 64 * sizeof(unsigned char)
    unsigned char *buf = USP malloc(IP_BUFFER_SIZE);
    if (!buf) {
        free(buf);
        return NULL;
    }
    DWORD read;

    if (!InternetReadFile(conn, buf, IP_BUFFER_SIZE, &read)) {
        InternetCloseHandle(conn);
        InternetCloseHandle(net);
        free(buf);
        return NULL;
    }
    InternetCloseHandle(conn);
    InternetCloseHandle(net);
    // Null terminate it if we have room
    if (read < IP_BUFFER_SIZE) {
        buf[read] = 0x00;
    } else {
        free(buf);
        return NULL;
    }
    return buf;
#undef IP_BUFFER_SIZE
#else
    // TODO: implement on linux
    return NULL;
#endif
}

unsigned char *getCurrentUser() {
#ifdef _WIN32
#define BUF_SIZE (UNLEN + 1) * sizeof(unsigned char)
    unsigned char *buf = USP malloc(BUF_SIZE);
    DWORD bufSize = BUF_SIZE;
    if (!GetUserName(SCP buf, &bufSize)) {
        free(buf);
        return NULL;
    }
    return buf;
#undef BUF_SIZE
#else
    // TODO: Implement and test on linux devices
    unsigned char * username = USP malloc(LOGIN_NAME_MAX * sizeof(unsigned char));
    getlogin_r(SCP username, LOGIN_NAME_MAX);
    return username;
#endif
}

unsigned char *getOS() {
#ifdef _WIN32
    char *buf = NULL;
    int len = GetFileVersionInfoSizeA("combase.dll", NULL);

    if (!len) {
        return NULL;
    }

    buf = (char *) malloc(len * sizeof(char));

    if (!buf) {
        return NULL;
    }

    if (!GetFileVersionInfo("combase.dll", 0, len, buf)) {
        return NULL;
    }

    UINT uLen;
    VS_FIXEDFILEINFO *lpFfi;
    BOOL bVer = VerQueryValue(buf, "\\", (LPVOID *) &lpFfi, &uLen);

    if (!bVer || uLen == 0) {
        free(buf);
        return NULL;
    }
    DWORD dwProductVersionMS = lpFfi->dwProductVersionMS;
    DWORD dwProductVersionLS = lpFfi->dwProductVersionLS;
    free(buf);
#define WINVER_SIZE 64
    unsigned char *winver = USP malloc(sizeof(unsigned char) * WINVER_SIZE);

    DWORD dwLeftMost = HIWORD(dwProductVersionMS);
    DWORD dwSecondLeft = LOWORD(dwProductVersionMS);
    DWORD dwSecondRight = HIWORD(dwProductVersionLS);
    DWORD dwRightMost = LOWORD(dwProductVersionLS);
    snprintf(SCP winver, WINVER_SIZE, "Windows %lu.%lu.%lu.%lu",
             dwLeftMost, dwSecondLeft, dwSecondRight, dwRightMost);
    return winver;
#undef WINVER_SIZE
#else
    // TODO: Implement and test on linux devices.
    return NULL;
#endif
}

unsigned char getAdminAccess() {
    BOOL isAdmin = FALSE;
#ifdef _WIN32
#if _WIN32_WINNT >= 0x0600
    DWORD bytesUsed = 0;
    TOKEN_ELEVATION_TYPE tokenElevationType;
    HANDLE token = GetCurrentProcessToken();

    if (!GetTokenInformation(token, TokenElevationType, &tokenElevationType, sizeof(tokenElevationType), &bytesUsed))
        return 'N';

    if (tokenElevationType == TokenElevationTypeLimited) {
        HANDLE hUnfilteredToken = NULL;

        if (!GetTokenInformation(token, TokenLinkedToken, &hUnfilteredToken, sizeof(HANDLE), &bytesUsed))
            return 'N';

        BYTE adminSID[SECURITY_MAX_SID_SIZE];

        DWORD sidSize = sizeof(adminSID);

        if (!CreateWellKnownSid(WinBuiltinAdministratorsSid, 0, &adminSID, &sidSize))
            return 'N';

        BOOL isMember = FALSE;

        if (!CheckTokenMembership(hUnfilteredToken, &adminSID, &isMember)) {
            const DWORD lastError = GetLastError();

            // Windows likes to be stupid and error when it actually succeeds...
            // Error code 0 is literally ERROR_SUCCESS: The operation completed successfully...
            // THEN WHY ERROR IN THE FIRST PLACE?
            if (lastError) {
                return 'N';
            }
        }

        isAdmin = (isMember != FALSE);
    } else {
        isAdmin = IsUserAnAdmin();
    }

#else
    isAdmin = IsUserAnAdmin();
#endif

#else
    // Nothing for linux as of yet
    // TODO: Implement for linux
#endif
    return isAdmin ? 'Y' : 'N';
}

unsigned char isElevated() {
#ifdef _WIN32
    BOOL fRet = FALSE;
    HANDLE hToken = NULL;
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken)) {
        TOKEN_ELEVATION Elevation;
        DWORD cbSize = sizeof(TOKEN_ELEVATION);
        if (GetTokenInformation(hToken, TokenElevation, &Elevation, sizeof(Elevation), &cbSize)) {
            fRet = Elevation.TokenIsElevated;
        }
    }
    if (hToken) {
        CloseHandle(hToken);
    }
    return fRet ? 'Y' : 'N';
#else
    // TODO: Implement for linux
    return 'N';
#endif
}
