/*
 * Simple file to test and debug the dll loader with.
 */
#include <stdio.h>
#include <Windows.h>

int main() {
    HMODULE dll = LoadLibrary(TEXT("Gravity.dll"));
    typedef void(__cdecl *runproc)();
    runproc run = (runproc) GetProcAddress(dll, "run");

    run();

    return 0;
}

int WINAPI WinMain(
        HINSTANCE hInstance,     /* [input] handle to current instance */
        HINSTANCE hPrevInstance, /* [input] handle to previous instance */
        LPSTR lpCmdLine,         /* [input] pointer to command line */
        int nCmdShow             /* [input] show state of window */
) {
    return main();
}
