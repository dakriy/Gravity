#ifndef GRAVITY_TUNNELMANAGER_H
#define GRAVITY_TUNNELMANAGER_H

#include "../shared/shared.h"
#include <stdbool.h>
#include "../shared/channelList.h"
#include "../shared/socketServer.h"

/**
 * Request a reverse tunnel from lport on the current device to rport on the ssh server
 *
 * @param lport
 * @param rport
 */
void startTunnel(int lport, int rport);

/**
 * Close and cleanup a tunnel specified by a local port
 *
 * @param lport
 */
void closeTunnel(int lport);

/**
 * Needs to be called when done using and after a call to setupTunnelManager
 *
 * @return 0 on success; < 0 on error
 */
int cleanupTunnelManager();

/**
 * Needs to be called before usage, and the return code checked before being used
 *
 * @return 0 on success; < 0 on error
 */
int setupTunnelManager();

#endif //GRAVITY_TUNNELMANAGER_H
