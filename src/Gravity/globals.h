#ifndef GRAVITY_GLOBALS_H
#define GRAVITY_GLOBALS_H

#include <stdbool.h>

// Default sleep time is in ms
#define DEFAULT_SLEEP_TIME 60 * 1000

extern int nextSleepTime;
extern bool done;

#endif //GRAVITY_GLOBALS_H
