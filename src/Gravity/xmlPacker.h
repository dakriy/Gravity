#ifndef GRAVITY_XMLPACKER_H
#define GRAVITY_XMLPACKER_H

#include "sessionHandler.h"

/**
 * Packs a computer info structure into a beacon and returns the XML of the beacon to be sent
 * returned XML buffer must be freed with a call to freeXMLBuffer by caller
 *
 * @param i
 * @return
 */
unsigned char *packBeacon(struct COMPUTER_INFO *i);

/**
 * Frees the xml string returned by packBeacon
 *
 * @param str
 */
void freeXMLBuffer(unsigned char *str);

/**
 * Unpacks the response to our beacon from the server.
 * The response is a list of tasks to be executed.
 *
 * @param xml the xml string, doesn't have to be null terminated, but length always needs to be passed
 * @param length length of the xml string
 * @return
 */
TaskList unpackBeaconResponse(unsigned char *xml, size_t length);

#endif //GRAVITY_XMLPACKER_H
