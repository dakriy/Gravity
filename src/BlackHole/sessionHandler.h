#ifndef GRAVITY_SESSIONHANDLER_H
#define GRAVITY_SESSIONHANDLER_H

#include "sshServer.h"
#include "../shared/shared.h"
#include "../shared/socketServer.h"

/**
 * Parses and incoming beacon and handles it appropriately
 * It is an ssh callback on a regular channel as we are only expecting
 * beacons to come in on regular channels at this time
 *
 * @param session
 * @param channel
 * @param data
 * @param len
 * @param is_stderr
 * @param userdata
 * @return
 */
int handleBeaconChannelData(ssh_session session,
                            ssh_channel channel,
                            void *data,
                            uint32_t len,
                            int is_stderr,
                            void *userdata);

/**
 * Forwards data on an ssh tunnel to the proper sockets.
 * This is an ssh callback
 *
 * @param session
 * @param channel
 * @param data
 * @param len
 * @param is_stderr
 * @param userdata
 * @return
 */
int handleTunnelChannelData(ssh_session session,
                            ssh_channel channel,
                            void *data,
                            uint32_t len,
                            int is_stderr,
                            void *userdata);

/**
 * Processes a tunnel close, marks the tunnel for removal from the tunnel list
 *
 * @param session
 * @param channel
 * @param userdata
 */
void handleTunnelClose(ssh_session session,
                       ssh_channel channel,
                       void *userdata);

/**
 * SSH callback that closes a channel. Marks the channel for deletion from the channel list.
 *
 * @param session
 * @param channel
 * @param userdata
 */
void handleChannelClose(ssh_session session,
                        ssh_channel channel,
                        void *userdata);


/**
 * This SSH Callback is for any ssh global requests such as requesting a tunnel
 *
 * @param session
 * @param message
 * @param userdata
 */
void globalRequestTrap(ssh_session session,
                       ssh_message message,
                       void *userdata);

/**
 * Takes an authenticated session and polls it for events until it closes
 * This is a blocking call, and will block until the session closes.
 *
 * @param s session container of the authenticated session
 */
void handleAuthenticatedSession(session_container s);

/**
 * Does any necessary work on the linked list such as closing channels marked for deletion
 *
 * @param session session to process channels on
 * @param root the root node in the linked list of channels
 */
void processChannels(ssh_session session, ChannelNode *root);

/**
 * Handles new connections on a socket when a reverse tunnel has been requested.
 *
 * @param node the ssh
 * @param session
 */
void serverSocketHandleCallback(ChannelNode node, ssh_session session);

#endif //GRAVITY_SESSIONHANDLER_H
