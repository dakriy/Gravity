#include <stdio.h>
#include <string.h>
#include <signal.h>
#include "sshServer.h"
#include <libxml/parser.h>

static volatile bool done = false;

int main() {
    // Initialize the xml parser
    xmlInitParser();
#ifdef DEBUG
    setbuf(stdout, 0);
#endif
    printf("Starting SSH Server...\n");
    int rc = initializeSSHServer();
    if (rc < 0) {
        printf("Could not initialize the SSH Server");
        return rc;
    }

    while (!done) {
        rc = listenForConnections();
        switch (rc) {
            case ERROR_NO_FREE_SESSIONS:
                printf("ERROR: Maximum number of sessions allocated, dropping connection.\n");
                break;
            case ERROR_NO_INSTANCE:
                printf("ERROR: Could not create session instance.\n");
                done = true;
                break;
            case ERROR_NO_CONNECTION_ACCEPT:
                printf("ERROR: Could not accept the connection\n");
                break;
            case ERROR_KEY_EXCHANGE_FAILED:
                printf("ERROR: Could not negotiate key exchange.\n");
                break;
            case ERROR_THREAD_HANDLER_FAILED:
                printf("ERROR: failed to start handler thread.\n");
                break;
            case NEW_CONNECTION:
                printf("New Connection Established\n");
                break;
            default:
                printf("Default case\n");
                break;
        }
    }

    stopSSHServer();
    xmlCleanupParser();

    return 0;
}
