#ifndef GRAVITY_SSHSERVER_H
#define GRAVITY_SSHSERVER_H

#include <libssh/libssh.h>
#include <libssh/server.h>
#include <sys/types.h>
#include <stdbool.h>
#include <libssh/callbacks.h>
#include "../shared/shared.h"
#include "../shared/channelList.h"

#define ERROR_NO_INSTANCE -1
#define ERROR_CANNOT_BIND_TO_SOCKET -2
#define ERROR_NO_FREE_SESSIONS -3
#define ERROR_NO_CONNECTION_ACCEPT -4
#define ERROR_KEY_EXCHANGE_FAILED -5
#define ERROR_THREAD_HANDLER_FAILED -6
#define SUCCESS 0
#define NEW_CONNECTION 1

/**
 * Tracked session metadata passed to ssh functions
 */
typedef struct SessionData {
    // Is session authenticated?
    bool auth;
    // Does the session have a currently allocated tty? (Not currently used)
    bool tty_allocated;
    // Linked list of the active channels
    ChannelNode list;
    // Intended size of beacon
    size_t beaconSize;
    // The ssh_session
    ssh_session session;
} *UserData;

/**
 * Thread and non session related data that is needed for the session to run
 * Each session has its own thread that handles the session, this metadata is thread and position information
 */
typedef struct SESSION_USAGE_CONTAINER {
    // Is this member in use? used when there is a new connection and we are finding an unused container
    bool in_use;
    // Current index in the array
    size_t index;
    // Has a thread been allocated for handling this session yet?
    bool threadAllocated;
    // thread handler tid
    pthread_t tid;
    // The tracked session specific metadata to go along with the session
    struct SessionData sessionData;
} *session_container;

/**
 * Called once at the beginning of usage, sets up the port binding and all that fun stuff.
 * Needs root if binding to a port < 1024
 *
 * @return 0 on success; < 0 on failure
 */
int initializeSSHServer();

/**
 * Listens on the bound socket for new connections, assigns them a session container, and starts them on a new thread
 * this call is blocking and will block until a new connection is established
 *
 * @return 0 on success; < 0 on failure;
 */
int listenForConnections();

/**
 * Unbinds the server from the socket and does any needed cleanup of active sessions
 */
void stopSSHServer();

/**
 * Authenticates the currently connecting user with a password
 *
 * @param user
 * @param password
 * @return true on success, false on error
 */
bool auth_user_password(const char *user, const char *password);

/**
 * SSH callback for authenticating a user
 *
 * @param s
 * @param user
 * @param password
 * @param userdata
 * @return SSH_AUTH_SUCCESS on success; SSH_AUTH_DENIED on failure
 */
int authenticate(ssh_session s, const char *user, const char *password, void *userdata);

/**
 * Grants a channel open request if the user is authorized to do so
 *
 * @param s
 * @param userdata
 * @return ssh_channel on success; NULL on failure
 */
ssh_channel handleNewChannelRequest(ssh_session s, void *userdata);

/**
 * Sets basic SSH Server callbacks
 *
 * @param s
 * @return
 */
int setServerCallbacks(session_container s);

/**
 * Sets basic SSH Session callbacks
 *
 * @param s
 * @return
 */
int setSessionCallbacks(session_container s);

//session_container getContainerFromSession(ssh_session s);

/**
 * Handles a connected user, opens a session with them as well as authenitcates them.
 * It also processes the authenticated session.
 *
 * @param arg
 */
void handleConnection(void *arg);

/**
 * Disconnects and closes the session
 *
 * @param c
 */
void closeSession(session_container c);

#endif //GRAVITY_SSHSERVER_H
