#ifndef GRAVITY_BEACONHANDLER_H
#define GRAVITY_BEACONHANDLER_H

#include <stdlib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libssh/libssh.h>

/**
 * Structure that holds one command to send
 * This is a linked list because at the time of getting the commands, it is unknown how many there will be
 */
typedef struct Command {
    // The ID from the db used for ordering
    unsigned long long id;
    unsigned char *command;
    unsigned char *parameter;
    struct Command *next;
} *CommandPtr;

/**
 * Handle an incoming beacon, and place the xml in the proper place to later be
 * processed by the web application (which routinely processes them and places them in the database)
 * The files placed in the folder have the timestamp that they came in on so that the appropriate time can be
 * added to the DB of when they were received.
 *
 * @param c
 * @param payload
 * @param len
 * @return 0 on success; < 0 on error
 */
int handleBeacon(ssh_channel c, unsigned char *payload, size_t len);

/**
 * Gets the tasks, formats them, and sends them back in reply to the beacon
 *
 * @param c
 * @param host
 * @param user
 * @return 0 on success; < 0 on error
 */
int sendBeaconReply(ssh_channel c, unsigned char *host, unsigned char *user);

/**
 * When a segment of tasks are sent, they need to be marked as such so that they are not sent again.
 * This function marks all tasks passed in from the database as sent. ID needs to be filled out in each command
 * structure.
 *
 * @param c The root node of the tasks to be marked as sent.
 * @return 0 on success; < 0 on error
 */
int updateSentFieldOnTasks(CommandPtr c);

/**
 * Retrieve the first matching node from an xpath expression.
 *
 * @param expression
 * @param doc
 * @return Pointer to the matching node. NULL if no node matches
 */
xmlNodePtr getFirstNodeFromXPath(unsigned char *expression, xmlDocPtr doc);

/**
 * Retrieve all current active tasks from the database for a host by host and running as user.
 * Returned list must be freed by caller with a call to freeCommandStructure.
 *
 * @param host Hostname of the device
 * @param user Currently running user of the device
 * @return
 */
CommandPtr getTasks(unsigned char *host, unsigned char *user);

/**
 * Packs a command list into xml.
 *
 * @param commands
 * @return
 */
unsigned char *packBeaconReply(CommandPtr commands);

/**
 * Frees the command list structure
 *
 * @param c
 */
void freeCommandStructure(CommandPtr c);

#endif //GRAVITY_BEACONHANDLER_H
