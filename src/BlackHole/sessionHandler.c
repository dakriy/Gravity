#include "sessionHandler.h"
#include "beaconHandler.h"
#include <mariadb/mysql.h>

static struct ssh_channel_callbacks_struct tunnelCallbacksStruct = {
        .channel_data_function = &handleTunnelChannelData,
        .channel_close_function = &handleTunnelClose
};

void handleAuthenticatedSession(session_container s) {
    ssh_event event = ssh_event_new();
    if (!event) {
        return;
    }

    // Attach the event to the session
    if (ssh_event_add_session(event, s->sessionData.session) == SSH_ERROR) {
        ssh_event_remove_session(event, s->sessionData.session);
        ssh_event_free(event);
        return;
    }

    while (1) {
        int ret = ssh_event_dopoll(event, 20);
        // Probably disconnected so we done
        if (ret == SSH_ERROR) {
            break;
        }
        processChannels(s->sessionData.session, &s->sessionData.list);
        handle_socket_tunnel_bindings(s->sessionData.session, &s->sessionData.list, serverSocketHandleCallback);
    }
    ssh_event_remove_session(event, s->sessionData.session);
    ssh_event_free(event);
}

int
handleBeaconChannelData(ssh_session session, ssh_channel channel, void *data, uint32_t len, int is_stderr,
                        void *userdata) {
    // Not enough to parse yet, just handle it when the rest of the data comes in
    if (len < BEACON_HEADER_SIZE) {
        return 0;
    }

    UserData session_data = (UserData) userdata;

    if (session_data->beaconSize) {
        // Get it on the next callback if it hasn't all come in yet
        if (len < session_data->beaconSize) {
            return 0;
        }
        printf("New Beacon!\n");
        handleBeacon(channel, data, session_data->beaconSize);
        int number_of_handled_bytes = session_data->beaconSize;
        session_data->beaconSize = 0;
        return number_of_handled_bytes;
    }
    session_data->beaconSize = *((size_t *) data);

    return BEACON_HEADER_SIZE +
           handleBeaconChannelData(session, channel, (((char *) data) + BEACON_HEADER_SIZE), len - BEACON_HEADER_SIZE,
                                   is_stderr, userdata);
}

void handleTunnelClose(ssh_session session, ssh_channel channel, void *userdata) {
    (void) session;
    (void) channel;
    ChannelNode node = (ChannelNode) userdata;
    node->closeChannel = true;
    printf("disconnected tunnel\n");
}

void handleChannelClose(ssh_session session, ssh_channel channel, void *userdata) {
    (void) session;
    UserData sc = (UserData) userdata;

    ChannelNode node = sc->list;

    while (node) {
        if (node->c == channel) {
            node->closeChannel = true;
            break;
        }
        node = node->next;
    }
}

void globalRequestTrap(ssh_session session, ssh_message message, void *userdata) {
    (void) session;
    UserData sd = (UserData) userdata;

    if (ssh_message_type(message) == SSH_REQUEST_GLOBAL) {
        switch (ssh_message_subtype(message)) {
            case SSH_GLOBAL_REQUEST_TCPIP_FORWARD: {
                printf("Reverse tunnel requested.\n");
                // Create a node
                ChannelNode n = createChannelNode();
                if (!n) {
                    ssh_message_reply_default(message);
                    break;
                }

                // Check the port they want
                int port = ssh_message_global_request_port(message);
                if (port < 1024) {
                    // Tell em no, couldn't make it
                    ssh_message_reply_default(message);
                    freeChannelNode(n);
                    break;
                }

                n->socket = make_listen_socket(port);
                if (n->socket == SOCKET_CREATION_FAILED) {
                    // Tell em no, couldn't make it
                    ssh_message_reply_default(message);
                    freeChannelNode(n);
                    break;
                }
                // Tunnel socket was successfully created
                n->attached = true;
                n->lport = port;
                // Mark the socket as a listen socket
                if (listen(n->socket, 5) < 0) {
                    ssh_message_reply_default(message);
                    freeChannelNode(n);
                    break;
                }

                // Add the channel uninitialized to the list
                // to be processed later
                // Send success.
                if (ssh_message_global_request_reply_success(message, port) == SSH_ERROR) {
                    ssh_message_reply_default(message);
                    freeChannelNode(n);
                    break;
                }

                sd->list = appendChannelToList(sd->list, n);
                break;
            }
            case SSH_GLOBAL_REQUEST_CANCEL_TCPIP_FORWARD: {
                printf("Reverse tunnel cancelled.\n");
                // Clean up the forward I guess
                // But how to do that?
                // Mark all tunnels for closing that need it
                int port = ssh_message_global_request_port(message);
                ChannelNode node = sd->list;
                while (node) {
                    if (node->lport == port) {
                        node->closeChannel = true;
                    }

                    node = node->next;
                }
                ssh_message_reply_default(message);
                break;
            }
            case SSH_GLOBAL_REQUEST_KEEPALIVE:
            case SSH_GLOBAL_REQUEST_UNKNOWN:
            default:
                ssh_message_reply_default(message);
                break;
        }
    }
}

int handleTunnelChannelData(ssh_session session, ssh_channel channel, void *data, uint32_t len, int is_stderr,
                            void *userdata) {
    (void) session;
    (void) channel;
    (void) is_stderr;
    ChannelNode node = (ChannelNode) userdata;

    // we are a tunnel, so send it along
    if (send(node->socket, data, len, MSG_NOSIGNAL) < 0) {
        node->closeChannel = true;
    }

    return len;
}

void processChannels(ssh_session session, ChannelNode *root) {
    ChannelNode cur = *root;

    while (cur) {
        // I think this is it for now lol.
        if (cur->closeChannel) {
            ChannelNode tmp = cur->next;
            *root = removeChannelFromListByNode(*root, cur);
            cur = tmp;
            continue;
        }
        cur = cur->next;
    }

}

void serverSocketHandleCallback(ChannelNode node, ssh_session session) {
    if (!node->c && node->attached) {
        /* Connection request on listen socket. */
        SOCKET_DESCRIPTOR new;
        new = accept(node->socket,
                     NULL,
                     NULL);
        if (new < 0) {
            // Accepting the socket failed, ignore it and move on
            return;
        }

        // Create a new channel for the connected socket in the tunnel
        ssh_channel c = ssh_channel_new(session);
        if (!c) {
            return;
        }
        ChannelNode n = addChannelNodeWithChannel(&node, c);
        if (!n) {
            // Welp drop the connection I guess...
            ssh_channel_free(c);
            return;
        }
        n->lport = node->lport;
        n->socket = new;
        n->attached = true;

        // There the channel is established and we can start sending/receiving
        // Only supporting localhost at this time
        // TODO: Get dynamic tunnels working
        if (ssh_channel_open_reverse_forward(n->c, "localhost", node->lport, "localhost", node->lport) == SSH_ERROR) {
            n->closeChannel = true;
            return;
        }
        // Now that we have a valid channel, lets create the callbacks for it
        n->cb = &tunnelCallbacksStruct;
        ssh_callbacks_init(n->cb)
        n->cb->userdata = n;
        ssh_set_channel_callbacks(n->c, n->cb);
    } else if (node->attached) {
        /* Data arriving on an already-connected socket. */

        // Write the data to the ssh channel...
        // buffer size of -4k- SOCKET_READ_BUFFER_SIZE?
        char BUFFER[SOCKET_READ_BUFFER_SIZE];
        int nbytes = recv(node->socket, BUFFER, SOCKET_READ_BUFFER_SIZE, 0);
        if (nbytes <= 0) {
            // Close the connection I suppose.
            node->closeChannel = true;
        } else {
            // Send that delicious data
            if (ssh_channel_write(node->c, BUFFER, nbytes) == SSH_ERROR) {
                // https://www.youtube.com/watch?v=9Deg7VrpHbM
                // Yes I really used a meme video transcript as a comment
                // Later edit when I forgot about this: the help being referred to was probably help closing the channel
                node->closeChannel = true;
                return;
            }
        }
    } else {
        // Why keep it open?? Mark for deletion
        node->closeChannel = true;
    }
}
